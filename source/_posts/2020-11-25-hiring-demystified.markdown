---
layout: post
title: "Hiring Demystified"
date: 2020-11-25 12:00:14 +0000
comments: true
categories: software
tags: speaking talks hiring companies pycon
sharing: true
keywords: hiring
description: "A talk at pyconES 2020 about engineering hiring practices"
author: Mar Bartolome
featured: false
published: true
---

It's not secret that the hardest problems in computer science are cache invalidation and naming things... oh, and hiring. In our industry we've developed a culture and a mysticism around hiring, with certain rituals and practices which are often so detached from reality that you see numerous jokes and memes about the subject. 

Hiring is a difficult problem, yet important to get right. Many developers are faced with the challenge of hiring other team members, without much clue into how to proceed, and end up just copying the well known rituals without stopping to analyse their effectiveness or implications. Often, this results in hindering both companies and candidates, especially those of under represented demographics. 

In this talk I gave at PyconES2020, I share my experiences and personal opinions both as a candidate and as an interviewer, analyze the implications of popular hiring tactics, and discuss what I consider effective ones, in order to hire the right developers for your team with minimum hassle for both sides.


<iframe width="560" height="315" src="https://www.youtube.com/embed/UBLTaQ_tRl4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>