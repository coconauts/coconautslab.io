---
layout: post
title: "Watchduino video and updates"
date: 2014-03-27 20:57:38 +0100
comments: true
categories: hardware
tags: watchduino
sharing: true
keywords: arduino,smartwatch,video,promotional,funny,apple
description: "Watchduino smartwatch promotional video"
---

We’ve made a demonstrational video showing the features of WatchDuino:

<iframe width="100%" height="450" src="//www.youtube.com/embed/CtgR1YiwnEY" frameborder="0" allowfullscreen></iframe>

We’ve also made major updates to the project documentation, detailing how to build your own.
You can look it up in the [project repo](https://bitbucket.org/rephus/watchduino/src/master/docs/how_to_replicate.md). 
We are in the process of refactoring the software to make it more framework-like, so making own your customizations will be a snap.
More updates on this soon!