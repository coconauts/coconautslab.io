---
layout: post
title: "Coconauts now is powered by Octopress"
date: 2014-05-10 11:44:38 +0100
comments: true
categories: general
tags: coconauts blog octopress
sharing: true
keywords: coconauts,octopress,wordpress
description: "Coconauts is powered by octopress"
---

You may have noticed that the design of our website has changed slightly recently. This is because we have migrated the software that powers it, from Wordpress to Octopress.

Wordpress is a very stable and easy to use blogging framework, written in PHP, and backed by a MySQL database. It's so easy to use in fact that is not uncommon to see it being used to power not only blogs,
but also simple websites that don require a lot of application logic. It's got a very friendly administration interface, so that once it's been set up it's very easy to use by
redactors without any tech knowledge.

<!--more-->

Wordpress has several problems though. First, all of your content must be held in a database, so it is cumbersome to extract it out of the blog, if you ever wanted to.
Dynamic websites are also less safe than static ones, and require more processing power to serve them. And if you ever tried migrating a Wordpress site, well, is definitely doable,
but not as simple as just copying and pasting your files and database from one server to another.

So recently, there's been a trend of going back to static websites when you don't need dynamic capabilities. But instead of writing the HTML pages directly, the new cool is using
a website generator: so you can write your context in a friendly markup language (like Markdown, or RestructuredText), and then call the software to generate the HTML website.
Such website is static, and can be hosted practically anywhere, even in Github Pages or Dropbox!

Writing your content in a markup language has several advantages over using a WYSIWYG editor in a webapp, and keeping the content in a database. To begin with, you don't need a database!
One thing less to worry about. Your blog content is perfectly accessible and readable in raw text files, without needing to go and query it out a database. Also, you can keep all
of your site's content under version control. How cool is that?

So for Coconauts we are now using Octopress, as we think is the more simple and convenient static website generator around. You write your content in Markdown (our favorite markup
language), use a command to generate your static site, and then your'r off to deploy it. In our case, we have a Jenkings polling our version control repo, so it auto-deploys it every
time we make a `git push`. Octopress also comes It comes with a lot of features built in, like Twitter buttons, or Google Analytics, and you can add more plugins or tweak all pages you want directly.

As they say, it's "A blogging framework for hackers" so if you feel brave enough [give it a try](http://octopress.org/).
