---
layout: post
title: "Super Time Bomb Halloween Updates"
date: 2014-10-30 19:30:00 +0100
comments: true
categories: games
tags: timebomb android halloween dlc
published: true
sharing: true
keywords: timebomb,dlc,halloween,android,free
description: "Free Android Halloween DLC"
---

We've got a tasty update ready for [project page](/projects/timebomb2), just in time for most spooky festivity
of the year. We know all of you love to dress up for Halloween, so now you can get a couple
of really scary outfits for Lollipop the bunny as well! We've included a really creepy 
zombie skin, and a very festive carved pumpking head.

<img src="/images/posts/zombie_bunny.png" /><img src="/images/posts/pumpkin_bunny.png" />

<!--more-->

But not only that, since the launching of Super Time Bomb we've been listening to all of
our users suggestions, and we've included some changes that
attempt to make the game more straightforward and fun:

- Instead of having to go though a tutorial, a tip system will guide you through learning
  all Super Time Bomb inside outs.
- Hovering tags will let you know what all of the menu items are about.
- Did you find boring to start from the begginning every time you died? worry no more!
  now you can skip 50 seconds ahead of time (by paying one little bunny in return).

So don't forget to update you game, or go to the 
[Google Play Store](https://play.google.com/store/apps/details?id=net.coconauts.timebomb2) 
to install it if you still don't have it. You are encouraged to leave us feedback on the
new features as well.

Have a spooky Halloween, and get lots of candies!