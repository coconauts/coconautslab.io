---
layout: post
title: "Microservices: the small print"
date: 2020-11-18 12:00:14 +0000
comments: true
categories: software
tags: microservices speaking talks pycon
sharing: true
keywords: microservices
description: "A talk at pyconES 2018 about the hidden cost of microservices"
author: Mar Bartolome
featured: true
published: true
---

Microservices get a lot of sales talk, which leads many teams to adopt them eagerly. However, people are not always aware that they come at a price.

In this talk I gave at PyconES 2018 I discuss the basic theory around microservices architectures, and go over the common pain points that they bring, and how they often get you in the opposite direction than you intended with them.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ch0A_lcaPG0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>