---
layout: post
title: "Spaceroads demo"
date: 2017-11-06 14:14:46 +0000
comments: true
categories: games
tags: unity3d spaceroads demo
sharing: true
keywords: spaceroads,demo,skyroads,unity3d,remake
description: "Spaceroads demo, skyroads remake on Unity3d"
author: Javier Rengel
featured: true
published: true
---

We announced a few months ago we were working on a remake of [Skyroads](http://www.bluemoon.ee/history/skyroads/), a 1993 arcade game, on Unity3D. We called it [Spaceroads](http://coconauts.net/blog/2017/07/25/spaceroads/).

Today we announce we have released a demo version you can play directly on your [browser](http://unity3d.coconauts.net/spaceroads-v10/)

![gameplay](/images/posts/2017-11-06-spaceroads-demo/v10-gameplay.gif)

<!--more-->

## Features

### Three adventure levels

We built the first 3 level of the original Skyroads in our editor, with the same layout, blocks and feels (the color might be different).

![gameplay](/images/posts/2017-11-06-spaceroads-demo/v10-gameplay.png)


## Level editor

We built a fully featured 3D editor for the game. You can add different block types, change start point, undo, preview levels or  publish them online. 

![gameplay](/images/posts/2017-11-06-spaceroads-demo/v10-editor.gif)

It has everything you need to build levels like in the original skyroads.

![editor](/images/posts/2017-11-06-spaceroads-demo/v10-editor.png)

You can even add effects (burning floor, recharge, speed up/down, etc)

![effects](/images/posts/2017-11-06-spaceroads-demo/v10-editor-effects.png)

Or color to the blocks

![color](/images/posts/2017-11-06-spaceroads-demo/v10-color.png)


Note: Saving levels or publishing online is built
but not supported on the demo version 


## Multiverse

Multiverse is not available on the demo, but this feature will allow players to list their built levels, and levels available online.

![multiverse](/images/posts/2017-11-06-spaceroads-demo/v10-multiverse.png)


[This version](http://unity3d.coconauts.net/spaceroads-v10/)
 is not complete and it can be a bit buggy, but we want you to test it and give us feedback before we continue with its development. So let us know what you think in the comments.

 ![spaceroads](/images/posts/2017-11-06-spaceroads-demo/v10-main-menu.png)
