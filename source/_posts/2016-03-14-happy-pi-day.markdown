---
layout: post
title: "Happy Pi Day!"
date: 2016-03-14 12:00:00 +0000
comments: true
tags: pi 3dprint
sharing: true
categories: general
keywords: pi,3dprint,cookies
description: "Happy Pi day"
author: Mar Bartolome
published: true
featured: false
---

Do you know what day is today? It's 3.14, and because it matches the
decimals in the most beloved irrational number ever, it's been called an
[official celebration of number Pi](https://en.wikipedia.org/wiki/Pi_Day).

Actually last year it was a most special instance, as it was 3.14.15; an
event that won't occur again for another 100 years! To celebrate, and to
give some use to our newly bought 3D printer, we printed some [Pi-shaped
cookie-cutters we found off Thingiverse](http://www.thingiverse.com/thing:28882),
and baked some delicious Pi... cookies.

<img src="https://thingiverse-production-new.s3.amazonaws.com/renders/42/94/15/17/a7/IMG_20150312_223708_preview_featured.jpg" />

This year still feels special, as it is 3.14.16, the decimals of *rounded* Pi.
If you wanna celebrate, and have a 3D printer yourself, have a look at
[this neat collection](http://www.thingiverse.com/thing:28882) they've put
together, and print yourself something awesome.
