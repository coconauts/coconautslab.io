---
layout: post
title: "Christmas update for Super Time Bomb"
date: 2014-12-01 22:00:43 +0000
comments: true
categories: games
tags: timebomb dlc christmas android
sharing: true
keywords: timebomb,dlc,christmas,android,free
description: "Free Android Christmas DLC"
---

Ho ho ho! Yes, December has just kicked in, so we've officially entered the
Christmas period now. Everywhere you keep seeing reindeers, Christmas trees,
people singing carols, mince pies (or perhaps turrón and mantecados, depending
where you live!)... and we though that you probably wouldn't want to 
leave the Christmas spirit behind just to play Super Time Bomb, right?

<!--more-->

So it's your lucky day then, because we've included two lovely Christmas
outfits for Lollipop today: a Santa's hat, and a Rudolph costume.

<img src="/images/posts/santa_bunny.png" /><img src="/images/posts/rudolph_bunny.png" />

Go to the [Google Play Store](https://play.google.com/store/apps/details?id=net.coconauts.timebomb2)
to update now! 

Lollipop and Coconauts wish you a very merry Christmas and a happy new year!
