---
layout: post
title: "Coconauts Podcast Pilot (en español)"
date: 2017-02-07 20:37:11 +0000
comments: true
keywords: podcast,español,oculus,zenimax,tinkerboard,libretaxi,scalebound,masters of doom,jukedeck
categories: podcast
tags: coconauts podcast español
published: true
---

Note: today's post is about a podcast we've started in spanish.
But don't worry, we'll be back to english on the next post!

Con ustedes, ¡el episodio piloto del podcast de Coconauts!

<iframe id='audio_16881889' frameborder='0' allowfullscreen='' scrolling='no' height='200' style='border:1px solid #EEE; box-sizing:border-box; width:100%;' src="https://gb.ivoox.com/es/player_ej_16881889_4_1.html?c1=ff6600"></iframe>

Ultimamente, dado que vamos al trabajo en un trayecto largo en tren, nos hemos
enganchado a escuchar podcasts (mayormente de videojuegos). Sin embargo,
no hemos encontrado ningun podcast de tecnologia o desarrollo en español
que nos guste, así que nos hemos planteado ¿por qué no intentar llenar el hueco?

<!--more-->

Nuestro plan con el podcast es un poco tratar los mismos temas que en el blog
(desarrollo software, hardware y de juegos) con una serie de seciones
mas o menos fijas.

En este episodio piloto hablamos de:

- Noticias: Zenimax vs Oculus, Asus Tinkerboard, Libretaxi y cancelacion de Scalebound.
- Yo he venido aqui a hablar de mi libro: Masters of Doom
- Debate: creatividad computacional

¡Esperamos que lo disfruteis! No dudeis en dejarnos comentarios aqui mismo,
en nuestro email, o en nuestra cuenta de twitter [@coconauts](https://twitter.com/coconauts).

Enlaces relacionados:

- [Oculus vs Zenimax](http://www.polygon.com/2017/2/1/14474198/oculus-lawsuit-verdict)
- [Asus Tinkerboard](https://www.engadget.com/2017/01/23/asus-tinker-board/)
- [Libretaxi (making of)](https://medium.com/@romanpushkin/how-i-made-uber-like-app-in-no-time-with-javascript-and-secret-sauce-94ef9120c7f6#.cc6jss25p)
- [Desarrolladores de scalebound sobre su cancelacion](http://uk.ign.com/articles/2017/01/11/scalebound-cancellation-developers-comment-on-the-situation)
- [Masters of Doom](https://www.amazon.es/Masters-Doom-Created-Transformed-Culture/dp/0749924896/ref=sr_1_1?ie=UTF8&qid=1486501773&sr=8-1&keywords=masters+of+doom)
- [Jukedeck @ London Techcrunch Disrupt 2015](https://www.youtube.com/watch?v=YY2FPWWc_Sk)
