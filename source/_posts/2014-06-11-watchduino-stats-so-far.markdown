---
layout: post
title: "WatchDuino stats so far"
date: 2014-06-11 20:34:38 +0100
comments: true
categories: hardware
tags: watchduino
published: true
sharing: true
keywords: smartwatch,win,contest,stats,2014
description: "Watchduino stats after winning contest"
---

A month has passed since we won the Biicode contest with our Watchduino project.
And thanks to Biicode and some other blogs around the world, we have received a lot of support for our project, 
so we would like to do a brief summary and stats about what happened this last month, and who mentioned us in the web.

<!--more-->

Results from Biicode were announced on 2014-04-30. We got 15 visitors that day at coconauts.net (powered by our previous blog in Wordpress), and 12 visitors on 24th (voting day perhaps?).

<img src="/images/posts/watchduino-stats/wordpress-stats.png" />

Our marketing team (le me) contacted [Hackaday](http://hackaday.com)
a few days later, and on 2014-05-07 they dedicated us [a blog post](http://hackaday.com/2014/05/06/tell-time-and-blink-an-led-on-your-wrist-with-watchduino/), along with a nice set of screenshots.
Thanks to this publication we received up to 882 visitors on a single day.
Also, as this page is a reference for hackers and DIY projects, our [Bitbucket repository](https://bitbucket.org/rephus/watchduino) received a lot of visitors too (428 to be precise), and we even got two forkers and seven watchers.

<img src="/images/posts/watchduino-stats/hackaday-stats.png" />

After being published by Hackaday, other blogs and forums followed, like [Gamebuino](http://gamebuino.com/forum/viewtopic.php?f=10&t=625&hilit=watchduino), the french blog [Semageek](http://www.semageek.com/watchduino-une-montre-open-hardware-sous-arduino/),  [Electronics lab](http://www.electronics-lab.com/blog/?p=28430) and even  [Atmel's](http://atmelcorporation.wordpress.com/2014/05/08/atmega328-powers-open-source-watchduino/) themselves.

If you want to see more details, like links and mentions, you can have a look at our [Twitter](http://twitter.com/coconauts) timeline.

Because of all this support we got from the Internet and hack community, we would like to continue developing on WatchDuino in the future, and work on a new version (with more features and a better look).


