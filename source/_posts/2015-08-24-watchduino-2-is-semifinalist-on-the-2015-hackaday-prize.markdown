---
layout: post
title: "WatchDuino 2 is semifinalist on the 2015 Hackaday Prize"
date: 2015-08-24 20:18:35 +0100
comments: true
sharing: true
author: Mar Bartolome
featured: false
published: true
categories: games
tags: watchduino arduino
keywords: watchduino,arduino,hackaday,prize,finalist,smartwatch
description: "WatchDuino 2 awarded as semifinalist on the 2015 Hackaday Prize"
---

<img src="/images/posts/hackaday_semifinal.png" />

We are really excited to announce that [WatchDuino 2](https://www.coconauts.net/projects/watchduino2/) has passed to
[the semi-finals of the 2015 edition of the Hackaday Prize](http://hackaday.com/2015/08/24/100-semifinalists-for-the-2015-hackaday-prize/)!
It was selected amongst the best 100 of a total of more than 900 projects.
To see this kind of recognition out of a community that we admire is
absolutely thrilling, thank you so much =)

This means that now we will have to give our best we want to be good enough
to be in the finals. Many other excellent projects are competing!

Keep an eye on the [project page on Hackaday](https://hackaday.io/project/7244-watchduino2),
as it is likely to be holding exciting updates and advancements during the
following month. And give us your skulls and likes if you have an account! =)
