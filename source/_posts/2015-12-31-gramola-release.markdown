---
layout: post
title: "Introducing Gramola, a lightweight web music player"
date: 2015-12-31 12:00:00 +0000
comments: true
tags: gramola software
sharing: true
categories: software
keywords: gramola,music,player,web,remote,node,nodejs,mp3
description: "Gramola, our web music player is now open source"
author: Mar Bartolome
published: true
featured: true
---

<img src="https://farm2.staticflickr.com/1513/24000467331_46d15821da_b_d.jpg" />

You may have noticed a mysterious project that has been since the dawn of time
in our front page: *"Gramola, lightweight online music player,
powered by JPlayer with tons of features"*. We even have a detailed
[project page](https://coconauts.net/projects/gramola/) about it...
yet disapointingly, you would find no download links or source code there.

We built (and *rebuilt*!) Gramola some time ago, and we've been wanting
to find some time to give it some polish and open source it. In fact,
at the begining of the year
[we promised you](https://coconauts.net/blog/2014/12/31/coconauts-2014-retrospective/)
that we would get it out in 2015. So to honor our words, here it is!

Read details about it on the [project page](https://coconauts.net/projects/gramola/),
or head directly to the [Github repository](https://github.com/coconauts/gramola)
for code and install instructions.

Happy music listening in 2016!
