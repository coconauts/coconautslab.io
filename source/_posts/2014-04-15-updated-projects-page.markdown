---
layout: post
title: "Updated projects page"
date: 2014-04-15 20:57:38 +0100
comments: true
categories: general
tags: coconauts projects
sharing: true
keywords: projects,coconauts,update
description: "Update projects page"
---

We have changed the design of our [projects](/projects) page, hopefully making it easier and quicker to have a glance to all of our projects, and their status. 
You may also notice that we’ve added a couple of new projects we just started working on.
We’ll add them their own page with more details once we mature them a bit, so just teasers for now!