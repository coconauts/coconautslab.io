---
layout: post
title: "Coconauts 2014 retrospective"
date: 2014-12-31 17:24:14 +0000
comments: true
categories: general
tags: coconauts
sharing: true
keywords: coconauts,2014,retrospective,summary
description: "Coconauts 2014 retrospective"
---

Every time there's a change of year we can't help taking a look back to
account for everything that is different from the same point one year ago.
For Coconauts 2014 has been a really exciting year, and we wanted to have a
record of our achievements during the same, so when we take a look back again
at the end of 2015 we have something to compare against.

<!--more-->

- We [won the Biicode 2014 competition with Watchduino](http://www.coconauts.net/blog/2014/04/30/watchduino-awarded-first-price-at-biicontest-2014/).
We are also [working on an improved version](http://www.coconauts.net/blog/2014/11/22/watchduino-2-dot-0-roadmap/),
so we'll have some more work for next year.
- We [migrated our blog from Wordpress to Octopress](http://www.coconauts.net/blog/2014/05/10/coconauts-is-powered-by-octopress/),
and we started blogging regularly on it.
- We [released Super Time Bomb](http://www.coconauts.net/blog/2014/09/15/super-time-bomb-is-out/),
our second LibGDX game for Android. We also did a couple of seasonal updates for it.
- You may have missed it, because we didn't blog about it, but we released a
[Web Game Manager](http://www.coconauts.net/projects/game-manager/) desktop app
(written in node.js).
- We opened a [Github account](https://github.com/coconauts), where we will
place all new pieces of public code from now on.
- Minor projects we've shown you include a [Chromium based screensaver](http://www.coconauts.net/blog/2014/08/02/web-screensaver/),
[wireless radio Arduino sensors](http://www.coconauts.net/blog/2014/09/03/send-readings-from-arduino-to-raspberry-via-radio/),
and a [meteorological station powered by a Raspberry Pi](http://www.coconauts.net/blog/2014/08/14/meteorological-station-with-rasbperry-pi-phase-1/).
- Internally, we have a much better organization and logistics. We started
using [Trello](http://trello.com) for task management, and [Jenkins and Ansible for
automated operations](http://www.coconauts.net/blog/2014/09/09/background-tasks-in-jenkins-ipchange/).
- We've also started a bunch of new projects, but they are still on a very early
stage, so we haven't shown anything about them yet. Hopefully some of them
will grow to a point where they can be published during the next year.

So we are quite happy to see the big leaps taken during the year. We expect 2015
to be at least just as exciting, if not even more! We are in fact going as far
as to state a series of commitments:

- To finally open source Gramola (we've actually rewritten it from scratch
  during the year!).
- To have a working Watchduino 2.0 prototype.
- To release at least another game or app.
- To blog more and better than we've done this year.

And on top of all this, we the Coconauts wish all our readers and followers
a happy and prosperous year 2015. We are really excited about it because...
it's finally the "future" according to the Back to the Future movies!
