---
layout: post
title: "Coconauts Podcast S1E1 (en español)"
date: 2017-02-19 20:00:14 +0000
comments: true
keywords: podcast,español,gitlab,gameband,rethinkdb,cloud spanner,jukedeck
categories: podcast
tags: coconauts podcast español
published: true
---

Note: today's post is about our podcast in spanish.
But don't worry, we'll be back to english on the next post!

¡Ya está aqui la segunda entrega de nuestro podcast!

<iframe id='audio_17098008' frameborder='0' allowfullscreen='' scrolling='no' height='200' style='border:1px solid #EEE; box-sizing:border-box; width:100%;' src="https://www.ivoox.com/player_ej_17098008_4_1.html?c1=ff6600"></iframe>


En este episodio hablamos de:

- Noticias: el outage de Gitlab, Steam cierra Greenlight, RethinkDB y Cloud Spanner, Gameband.
- Yo he venido aquí a hablar de mi libro: la psicología de los objetos cotidianos de Don Norman
- Debate: frameworks de desarrollo de videojuegos

<!--more-->

Enlaces relacionados:

- [Gitlab outage postmortem](https://about.gitlab.com/2017/02/10/postmortem-of-database-outage-of-january-31/)
- [Steam greenlight](http://steamcommunity.com/greenlight/discussions/18446744073709551615/133256758580075301/)
- [RethinkDB](https://rethinkdb.com/blog/rethinkdb-joins-linux-foundation/)
- [Google Cloud Spanner](https://cloudplatform.googleblog.com/2017/02/introducing-Cloud-Spanner-a-global-database-service-for-mission-critical-applications.html)
- [Gameband](https://www.kickstarter.com/projects/gameband/gameband-the-first-smartwatch-for-gamers?ref=category_newest)
- [Watchduino](https://www.youtube.com/watch?v=CtgR1YiwnEY)
- [La psicologia de los objetos cotidianos](https://www.amazon.es/psicolog%C3%ADa-objetos-cotidianos-Serie-Media/dp/8415042019)
- [Comparativa de game frameworks](http://coconauts.net/blog/2017/01/09/2d-game-framework-comparison/)

Música de http://jukedeck.com
