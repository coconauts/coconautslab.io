---
layout: post
title: "Coconauts 2017 Retrospective"
date: 2018-01-18 19:14:46 +0000
comments: true
categories: general
tags: coconauts retrospective
sharing: true
keywords: escape room,spaceroads,podcast,hackdays
description: "A look back at 2017, and plans for the year ahead"
author: Mar Bartolome
featured: false
published: true
---

It's the fourth year we're doing this, my, time flies!

2017 was an incredibly packed year for us at a personal level, and the fun is still to continue in 2018. We still managed to scratch a bit of time for Coconauts, with highlights being:

- The start of a [podcast](http://coconauts.net/blog/categories/podcast/). 
We only recorded three episodes, but each of them managed a listener count of over 100 on
[ivoox](https://www.ivoox.com/podcast-coconauts_sq_f1385224_1.html) alone.
- We started setting up [hackdays](https://coconauts.net/blog/tags/hackday/)
during the local bank holidays, which brought us: 
[a escape room game](https://coconauts.net/blog/2017/05/29/bank-holiday-hackday-escape-room-on-unity3d/) 
and [algorithmic trading experiments](https://coconauts.net/blog/2017/08/29/bank-holiday-hackday-algorithmic-trading/).
- On the hardware front, [energenies](https://coconauts.net/blog/tags/energenie/) 
have been our favorite toys. Worthy of mention are also a 
[live picture frame](https://coconauts.net/blog/2017/11/27/live-picture/) (featured on 
[Instructables](http://www.instructables.com/id/Live-Picture-Frame-With-Raspberry-PI/)) and a 
[smart LED-illuminated staircase](https://coconauts.net/blog/2017/02/21/stair-lights/).
- Gamedev didn't stop this year either. We did a bit of fiddling with 
[Phaser](https://coconauts.net/projects/rps/) and 
[Cocos2D](https://coconauts.net/blog/2017/10/30/proc-websockets/), 
but in the end we keep coming back to Unity3D for anything serious. 
Aside from the already mentioned [Escape Room](https://coconauts.net/projects/escaperoom/),
 we've done a [Skyroads remake](https://coconauts.net/projects/spaceroads/).
- What about purely software projects? Two little handy utilities: a 
[Github PR tracker](https://coconauts.net/projects/github-pr-tracker/) and an 
[infinite playlist generator for Spotify](https://coconauts.net/projects/infinify/).
- And last but not least: we've broken a record of blogposts this year! 
with 21 of them, almost two per month. 

Hoping all of our readers enjoyed following us during 2017. It's still not
too late to whish everyone a happy 2018, and stay tuned for more exciting 
stuff to happen over at Coconauts!