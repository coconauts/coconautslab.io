---
layout: post
title: "Lightning-dodge-O-matic"
date: 2016-06-13 20:51:00 +0000
comments: true
tags: arduino servo
sharing: true
categories: hardware games
keywords: final fantasy x,final fantasy 10,lightning,lulu,dodge,ffx,arduino,servo,ldr,psvita
description: "Arduino device to dodge lightnings on Final Fantasy X "
author: Javier Rengel
published: true
featured: true
---

If you have played Final Fantasy X, either its original version for PS2 or its remastered version
for PS4 and PSVita, you might remember Thunder Plains and its lightning dodge challenge
to get the Mars sigil.

It's quite challenging and it might take a while. I once got it (and I have a PS2 memory card to prove it),
but I was not in the mood to do it again on the remastered version.

That's why I decided to build a simple machine to do the job for me.
Because constantly processing inputs to produce outputs is what machines do best.

<blockquote class="twitter-video" data-lang="en"><p lang="en" dir="ltr">Lightning dancing like a machine. Presenting lightning-dodge-O-matic <a href="https://twitter.com/hashtag/FFX?src=hash">#FFX</a> <a href="https://twitter.com/hashtag/Lulu?src=hash">#Lulu</a> <a href="https://twitter.com/hashtag/Arduino?src=hash">#Arduino</a> <a href="https://t.co/WMLX1CRwSN">pic.twitter.com/WMLX1CRwSN</a></p>&mdash; Javi Rengel (@rephus) <a href="https://twitter.com/rephus/status/742057060456648709">June 12, 2016</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<!--more-->

Just by using a LDR, a servo and an Arduino you can dodge lightnings like a Pro too.

You can download the script and the schema in [Github](https://github.com/rephus/lightning-dodge-O-matic/blob/master/README.md)

![Schema](https://farm8.staticflickr.com/7335/27042150163_732cb2c99b_z_d.jpg)

By building this, you can avoid not only 200 lightnings but as many you like, I did 1066 in a few hours.

![record](https://farm8.staticflickr.com/7423/27042150153_b6daeed7d5_z_d.jpg)

Happy gaming.
