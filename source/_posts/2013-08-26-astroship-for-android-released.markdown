---
layout: post
title: "Astroship for Android released"
date: 2013-08-26 20:57:38 +0100
comments: true
sharing: false
tags: android game astroship
categories: games
sharing: true
keywords: astroship,atari,game,android,free
description: "Astroship, free asteroid-inspired game for android"
published: false
---
We are proud to present our first Reverse Game for Android, available right now to download for free from the Google Play store.

Find more information on the [project page](http://www.coconauts.net/web/?page_id=19),
or on the [Google Play page](https://play.google.com/store/apps/details?id=net.coconauts.reverse.asteroids).

## Update (2014-05-01)

The app has been removed from the Google Play Store for an alleged copyright infringement. 
It is true that the game was a tribute to the original Asteroids game from Atari with a 
**completely different gameplay** and without any kind of **profit**.

Maybe we should feel honored that Atari and it's efficient team of attorneys have considered
Astroship to be a worthy competitor for their game.
