---
layout: post
title: "Watchduino entered on Biicontest 2014"
date: 2014-03-05 20:57:38 +0100
comments: true
categories: hardware
tags: watchduino
sharing: true
keywords: smartwatch,arduino,contest,2014
description: "Smartwatch entered contest 2014 for arduino projects"
---

We’ve entered [WatchDuino](http://www.coconauts.net/web/?page_id=112) into the [Arduino/Raspberry Pi competition](https://www.biicode.com/biicontest2014-en) 
that the folks at Biicode have set up. We expect it to be a good opportunity to give the project some visibility,
as well as a good excuse to give the development a little push. 
We hope that our development efforts for the project will make it more reusable and easy to play with.

Biicode is both a dependency management systems (for now focused for C/C++, but increased support for more languages is planned), 
and a central repository to hold such dependencies. [Check them out!](https://www.biicode.com/)

{% img  /images/posts/watchduino.png 350 350 %}

In the following weeks we will post a demonstrative video showing WatchDuino’s features, and we will improve the documentation. Stay tuned!