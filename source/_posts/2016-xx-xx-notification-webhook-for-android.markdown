---
layout: post
title: "Notification webhook app for Android"
date: 2016-06-13 20:51:00 +0000
comments: true
tags: android
sharing: true
categories: software
keywords: cordova,notification,webhook,android
description: "Notification webhook app for Android"
author: Javier Rengel
published: true
featured: false
---

In a few of our projects, we wanted to get notifications from our phone into our web apps easily. So we thought about building a simple Android app in Cordova to redirect notifications to a webhook.

![Notification icon](https://github.com/rephus/notification-webhook/blob/master/www/img/logo.png?raw=true)

<!--more-->
That's how we built NotificationHook, you can download it from the [Play store](TODO) or build and upload your own version from [Github](https://github.com/rephus/notification-webhook)

![Screenshot](https://github.com/rephus/notification-webhook/blob/master/screens/screen-small.png)

Once installed, you need to give permissions to the app in
`Settings > Notification > Notification Access`

And don't worry, we don't send any private info to anyone, only to your own webhook.

Really handy for your quick hacks.
