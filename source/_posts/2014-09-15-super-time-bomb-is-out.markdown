---
layout: post
title: "Super Time Bomb is out!"
date: 2014-09-15 19:30:00 +0100
comments: true
categories: games
tags: timebomb android
published: true
sharing: true
keywords: timebomb,game,casual,android,free
description: "New game release, free Android game"
---

We're proud to announce the release of a new Android game, that we've been developing
for quite some time: *Super Time Bomb*.

<img src="/images/posts/supertimebomb.png" />

<!--more-->

Super Time Bomb attempts to be an old school game in every possible aspect: simple
infinite mechanics, pixelated graphics, chiptune music, a difficulty curve specially
tailored to maximize addiction, and just plain fun. And bunnies! And explosions! 
What's not to love?

And on top of that, the game is completely free! And we don't just mean free as in this
modern "free to play" concept where you download a free game where the mechanics are 
completely broken unless you pay real money to buy game extras. No, when we say that 
*Super Time Bomb* is old school we also mean it money-wise: you won't have to pay a 
dime to play it, before or after you download it.

So what are you waiting for? If you're an Android user, head over to the 
[Google Play Store](https://play.google.com/store/apps/details?id=net.coconauts.timebomb2) 
right now, and start enjoying *Super Time Bomb*.

Learn more about the game in the [project page](/projects/timebomb2), and be completely
welcome if you want to drop us any feedback. If you enjoy the game, please also consider leaving us a tip using the in-app donation button.

