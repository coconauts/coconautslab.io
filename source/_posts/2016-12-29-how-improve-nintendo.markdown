---
layout: post
title: "How to improve classic Nintendo games"
date: 2016-12-29 10:42:19 +0000
comments: true
categories: games
tags: nintendo mini mario achievements multiplayer
sharing: true
keywords: nintendo,mini,mario,achievements,multiplayer
description: "How can Nintendo improve the Nintendo Mini"
author: Javier Rengel
featured: false
published: true
---

Although the Nintendo Mini has been a great success, and the best way of recreating our childhood
experience with a great collection of classic Nintendo games; There are a lot of things still Nintendo
can do to our beloved classic games in order to let us play those classic games in a complete new (and refreshing) way. They don't even need to do it on a new old mini console, but on their virtual store on WiiU or NX (switch)

<img src="/images/posts/2016-12-29-how-improve-nintendo/nes-mini.jpg" alt="achievement">

<!--more-->

Some of these ideas have already been proved possible in our [Retrophies](html5.coconauts.net/retrophies) project, just by taking the original game, with no ROM hacking involved but just checking the memory values in the emulator, so imagine what Nintendo can do with their original games,
something they already tried on the great Nes Remix compilation.

## Achievements

Some people love them, some people hate them, but it's true they are a good way of motivating people
on playing games again with an objective in mind. Won't you play Super Mario Bros again if I tell you
you will get an official reward by finishing it ? Maybe the brand new Nintendo coins.

<img src="/images/posts/2016-12-29-how-improve-nintendo/achievement.png" alt="achievement">

This was the main feature of Retrophies and it was pretty fun to build and play.

## Multiplayer

Another concept we tested on Retrophies, having the capability of see (or interact) with
other players in real time while you play.

<img src="/images/posts/2016-12-29-how-improve-nintendo/multiplayer.png" alt="multiplayer">

Sounds like playing New Super Mario Bros on Super Mario Bros.

## Scoreboards

Who is the fastest player to finish World 1-1 on Super Mario Bros ? This is a concept we
are still working on Retrophies.

Nintendo can add official scoreboard to most of their games. I mean, speedrunners
do this with most Nintendo games already, think about all the crazy Ocarina of Time speedrunnes available, why don't make it official.

## Maker version of classic games

Remember Mario maker and how fun was to build scenarios on Super Mario ? Then why not integrating
this function on existing games ? This is also a possibility that we could integrate in
Retrophies in the future (by adding blocks or items in the screen in memory).

Do you have another idea for twisted classic Nintendo games ?
