---
layout: post
title: "Infinify service"
date: 2017-10-16 14:57:46 +0000
comments: true
categories: software
tags: infinify spotify nodejs music
sharing: true
keywords: free,infinify,spotify,service,music,discovery,infinite,nodejs,javascript
description: "Infinify service spotify recommender"
author: Javier Rengel
featured: false
published: true
---

A few months ago, we implemented, during one of our hackdays, an [infinite recommender for Spotify](/blog/2017/05/01/infinify-an-infinite-discovery-mode-for-spotify/) for private usage.

Today, we announce we've just published this service to the public for FREE, allowing multi-user accounts using OAUTH, so you can use Infinify without installing or configuring the service by yourself.

![Infinify](/images/posts/2017-05-01-infinify/infinify.png)

This service is available on [infinify.coconauts.net](http://infinify.coconauts.net) and you just need to identify in the app using your Spotify account. 

