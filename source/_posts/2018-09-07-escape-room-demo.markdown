---
layout: post
title: "Escaperoom x unlock"
date: 2018-09-07 14:00:14 +0000
comments: true
categories: games
tags: unity3d escaperoom unlock
sharing: true
keywords: escaperoom,unity3d
description: "Escaperoom game on unity3d based on unlock! level design"
author: Javier Rengel
featured: false
published: true
---

A year ago we showed you a [generative escaperoom]() game on Unity3D. Based on that formula, we decided to create some manually-crafted levels to make the game even better. So we decided to copy the first level design of a card based escaperoom game called [Unlock](https://boardgamegeek.com/boardgame/213460/unlock-escape-adventures). And the result was as good as we expected.

![unlock](/images/posts/2018-09-07-escape-room-demo/unlock-cards.jpg)

![uniy3d-design](/images/posts/2018-09-07-escape-room-demo/overview.png)

The game is not yet finished, but because we are going to move to a different project, we thought about showing you a demo of what we've built so far.

<!--more-->

## Demo 

This is a first look of the mechanics of the game

![demo](/images/posts/2018-09-07-escape-room-demo/level1.gif)

You can play this demo in here http://unity3d.coconauts.net/escaperoom-unlock/. Remember, it is not finished so it could be buggy and you will not be able to finish the level.

## Level design 

We copied the gameplay of the first level of the unlock game, with the same clues, same objects, similar text entries  and even the similar wall textures. 

![main-room](/images/posts/2018-09-07-escape-room-demo/main-room.png)

## The inventory system 

We added to this game a new inventory wheel system, where you pick items and you use them on any other object in the scene using the right click, showing all the items you have on a wheel.

![uniy3d-design](/images/posts/2018-09-07-escape-room-demo/inventory.gif)

But of course, different items will have a different effect on the item you're trying to use it on.

## Conclussions

It takes a lot to generate the 3d models, implement the logic and replace the card-only mechanics into a 3D videogame; but the result is worth it. 

I think, (now that we have proved the concept with a working level design) we could start building our own  levels, adding more interactive puzzles (like the laptop one in our previous game). 

However, we will pause this project for a while to start working on different stuff, but let us know if you have any interest on playing a game like this at some point in the future.

What do you think about the game?
Your feedback is really valuable to us! so please let us know on the comments or [on twitter](http://twitter.com/coconauts).
