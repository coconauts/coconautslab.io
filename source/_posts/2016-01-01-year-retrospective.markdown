---
layout: post
title: "Coconauts 2015 retrospective"
date: 2016-01-01 14:00:00 +0000
comments: true
tags: coconauts retrospective
sharing: true
categories: general
keywords: gramola,watchduino,retrospective,unity3D
description: "A look back a 2015, and plans for the year ahead"
author: Mar Bartolome
published: true
featured: true
---

Year 2015 has come to an end, and as it turns out we do have
[hoverboards](https://en.wikipedia.org/wiki/Self-balancing_two-wheeled_board),
[holograms/augmented reality](https://www.youtube.com/watch?v=vZRFcGrrsyc),
yet for some reason hip-kids insist in dressing as lumberjacks rather than
in futuristic clothing.

It feels just like yesterday when we were writing
[our retrospective post](https://coconauts.net/blog/2014/12/31/coconauts-2014-retrospective/)
last year. Time does fly! So how did we fare this year in comparison?

<!-- more -->

- The [second version of Watchduino](https://coconauts.net/projects/watchduino2/)
  was an absolute smash. We managed to get a functional prototype with
  Bluetooth connectivity, plus a phone companion app. It's starting to
  look something like a real smartwatch, and it was awarded
  [semifinalist in the Hackaday Prize](https://coconauts.net/blog/2015/08/24/watchduino-2-is-semifinalist-on-the-2015-hackaday-prize/).
- Our second big release of the year, even if it was a bit last minute,
  was the long promised open sourcing of
  [Gramola](https://coconauts.net/blog/2015/12/31/gramola-release/),
  a web-based music player.
- Smaller projects, but let's not forget about:
  [a notification listener plugin for Cordova](https://coconauts.net/blog/2015/10/13/notification-listener-android-cordova/),
  [an Arduino automated watering system](https://coconauts.net/blog/2015/04/22/simple-automatic-watering-system-arduino/),
  our [404-games](https://coconauts.net/blog/2015/07/31/404-games/).
- Seems like we still aren't blogging as much as we should, as we have some
  extra small projects that we didn't tell you about in the blog:
  [an image-to-json](https://github.com/coconauts/image-to-json) converter,
  [a generative game](http://itch.io/jam/procjam/rate/43682) made for the
  [PROCJAM](http://www.procjam.com/) game jam, and
  [a webapp to find open restaurants near you](https://github.com/coconauts/find-open).
- We didn't blog as much as we would have liked, but even so we brought
  you a bunch of tutorials about
  [Arduino](https://coconauts.net/blog/tags/arduino/),
  [Unity3D](https://coconauts.net/blog/tags/unity3d/), or
  [chiptune music](https://coconauts.net/blog/tags/chiptune/), among others.

The year certainly felt as packed as the year before, so we definitely
want to keep the momentum going for 2016. Here's our immediate plans for
the year to come:

- We didn't do as much gamedev as we would have liked during 2015, so we
  would like to finish a Unity3D game during the next year. Specially now that
  [Unity3D is available for Linux](http://blogs.unity3d.com/es/2015/08/26/unity-comes-to-linux-experimental-build-now-available/),
  there's no better time!
- Once again, we want to aim to do more and better posting. 2015 saw
  regular blog updates until october, but for this year we want to do
  even better.
- To help fatten our blog and Github account, we want to commit to talk
  about our projects even while in an early stage. Instead of doing the
  development of a game or project in the dark until it is polished enough to
  show (as done with [Exploding Bunnies](https://coconauts.net/projects/timebomb2/)
  or [Gramola](https://coconauts.net/projects/gramola/))
  we want to to show prototypes and betas as early as we
  have them, and keep our development more open
  (as we have done with [Watchduino2](https://coconauts.net/projects/watchduino2/)).
- Speaking of Watchduino2, we plan to continue it's development due to
  popular demand, albeit perhaps at a slower pace than we did during 2015.

Wishing a happy and exciting 2016 for all our followers!
