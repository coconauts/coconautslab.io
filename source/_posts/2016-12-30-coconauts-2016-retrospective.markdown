---
layout: post
title: "Coconauts 2016 retrospective"
date: 2016-12-30 21:11:39 +0000
comments: true
tags: coconauts retrospective
sharing: true
categories: general
keywords: retrospective,retrophies
description: "A look back at 2016, and plans for the year ahead"
author: Mar Bartolome
published: true
featured: false
---

Another year went by, and this is the third that we find ourselves writing
a retrospective in the blog, not bad eh?
[2016 hasn't been too cheerful](https://www.youtube.com/watch?v=Z04M6NhkIKk)
(at least not for fans of the many stars that have left us this year,
or for people with progressive political views in general).

When it comes to Coconauts however it didn't go too bad:

- Perhaps the biggest highlight of the year is the
  [Retrophies project](http://html5.coconauts.net/retrophies). Featured in
  [HackerNews](https://news.ycombinator.com/item?id=12205560) and
  [Reddit](https://www.reddit.com/r/emulation/comments/4voi81/retrophies_achievements_system_for_emulators/).
  We started it as an experiment that turned out to be successful,
  and we are still expanding it (for instance, the latest addition was a
  [multiplayer mario game using websockets](http://coconauts.net/blog/2016/12/29/how-improve-nintendo/)).
- We've been experimenting with the ESP8266 chip, as a more versatile,
  powerful and cheaper alternative to Arduino, and we started
  [migrating Watchduino to the ESP8266](http://coconauts.net/blog/2016/09/08/smartwatch-prototype-with-esp8266/http://coconauts.net/blog/2016/09/08/smartwatch-prototype-with-esp8266/).
- A small hack that also turned out to be a hit is the
  [Lightning-dodge-O-matic](http://coconauts.net/blog/2016/06/13/lightning-dodge-o-matic/),
  featured on [Hackaday](http://hackaday.com/2016/06/22/cheating-at-video-games-arduino-edition/http://hackaday.com/2016/06/22/cheating-at-video-games-arduino-edition/)!
- We published our [custom mini-CI system](http://coconauts.net/blog/2016/02/04/coconauts-ci-nodejs/)
- We started the year working on a [mobile version of Starcraft](http://coconauts.net/blog/2016/01/30/starcraft-mobile-unity3d/http://coconauts.net/blog/2016/01/30/starcraft-mobile-unity3d/) (which we shortly after stopped working on, but there's still a demo video).

Our plans of focusing on game making haven't materialized as much as we would have liked.
We've also continued blogging very little during the year; although, there is one
aspect where we have delivered: more frequent status updates about ongoing projects. Thanks to this,
we've shown you some prototyes and smaller projects through the year despite not
having any big reveals.

So what are our best whishes for 2017? Pretty much the same as the year before:

- More games
- More open source
- More frequent blogging
- More awesome

See you in 2017, have a good one! =)
