---
layout: post
title: "Creating TRVE CHIPTUNE with a tracker"
date: 2015-04-19 19:34:31 +0000
comments: true
categories: games
tags: chiptune timebomb music
author: Mar Bartolome
published: true
---

Following up with the 
[previous post about chiptune music](https://www.coconauts.net/blog/2015/02/02/creating-chiptune-style-music-with-ubuntu/),
remember what I said about those purists only considering to be TRVE
CHIPTUNE that in the format of Amiga MOD files created with an old
school tracker software? Well, guess what, you can do just that to 
create chiptune music the hardcore way.

<!--more-->

This route is certainly not for the faints of heart, but the results
will be really convincing and proper chiptune music, not cheap
imitations. As a matter of fact, when you go out in the wild looking for
chiptune tutorials they are mostly going to point to this approach.

So in the dawn of times, [apparently](http://en.wikipedia.org/wiki/Chiptune#Tracker_chiptunes)
Amiga computers had really good sound chips and equally good tracking
software, and this spawned a community of enthusiasts creating chiptune
music with them. Over the years, software trackers came to be for PC,
imitating those of Amiga computers, and these are the ones you can use
to create chiptune music, as was done in the 80s. And with that, I mean
cryptic and hardcore text-based interfaces. Like this one:

<iframe width="420" height="315" src="https://www.youtube.com/embed/Fw4Aa0FfuJU" frameborder="0" allowfullscreen></iframe>

That was [MilkyTracker](http://milkytracker.org/), which is actually
the friendliest of trackers that I found (it's also free and available
for Linux). There are [plenty of other options out there though](http://woolyss.com/chipmusic-chiptrackers.php).

If you feel brave enough to step into this path, your experience
wouldn't be complete without some ancient tutorials in glorious static
html websites from a decade ago, so here's some to get you started:

- [The Tracker's Handbook](http://resources.openmpt.org/tracker_handbook/handbook.htm)
- [Amiga Music Preservation FAQ](http://amp.dascene.net/faq.php)
- [Tutorial at The ChipWIN blog](http://chiptuneswin.com/blog/so-you-wanna-make-a-chiptune-2-computer-trackers/)
- [Woolys](http://woolyss.com/) is an extensive reference site with tons
of tutorials and documentation about all things chiptune and tracking.

Don't forget though that there's a saner way to do it, we've shown you how in the
[previous post](https://www.coconauts.net/blog/2015/02/02/creating-chiptune-style-music-with-ubuntu/)!
