---
layout: post
title: "We built our house with Unity3D and Blender"
date: 2016-05-19 22:51:00 +0000
comments: true
tags: unity3d blender game house fps
sharing: true
categories: games
keywords: unity3d,blender,game,house,fps
description: "We built our house with Unity3D and Blender"
author: Javier Rengel
published: true
featured: true
---

We moved home a few months ago to a beautiful house in the country side in UK,
outside the expensive area of London, but close enough to commute daily.

As soon as we moved in, we built a simple version of our home in 3D on Blender
 to help us decorate. And then we integrated the 3D model into Unity3D and build
a game on top of it, on this case, a FPS or more like a FPW: First Person Walker,
a FPS without shooting, not as fun as it sounds.

We posted an image on Twitter a few months ago about our first prototype:

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">I finally created a First person game of my new home with <a href="https://twitter.com/hashtag/unity3D?src=hash">#unity3D</a> and <a href="https://twitter.com/hashtag/blender?src=hash">#blender</a> . Looks better than real life! <a href="https://t.co/JjSPPP1835">pic.twitter.com/JjSPPP1835</a></p>&mdash; Javi Rengel (@rephus) <a href="https://twitter.com/rephus/status/698998470280089600">February 14, 2016</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<!-- more -->

And this is how our place looks after we bought all the furniture.

<img src='https://farm8.staticflickr.com/7236/26513350403_4ac3cc142c_z_d.jpg'/>
<img src='https://farm8.staticflickr.com/7714/27083818276_36950ff772_z_d.jpg'/>

<img src='https://farm8.staticflickr.com/7715/27083819056_acb9ae8fa5_z_d.jpg'/>
<img src='https://farm8.staticflickr.com/7348/27083818686_12b5f68383_z_d.jpg'/>

<img src='https://farm8.staticflickr.com/7211/27083819206_727a82707c_z_d.jpg'/>
<img src='https://farm8.staticflickr.com/7434/26513349443_4d9df59b0d_z_d.jpg'/>

<img src='https://farm8.staticflickr.com/7750/27083819266_15ca7674e5_z_d.jpg'/>
<img src='https://farm8.staticflickr.com/7054/27083818506_f84f07ba12_z_d.jpg'/>

The game is also a good chance to try out VR, perfect to use with Google VR to feel
like you are at home.

<img src='https://farm8.staticflickr.com/7554/27083819136_dd3a9a50d3_z_d.jpg'/>

If you are interested, you can play on our 3D model we made, please feel at home.

[Play it here!](http://unity3d.coconauts.net/welwyn)
