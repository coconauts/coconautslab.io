---
layout: post
title: "RPS: A Phaser.io Simple RTS"
date: 2017-02-18 11:57:19 +0000
comments: true
categories: games
tags: html5 phaser.io RPS RTS games
sharing: true
keywords: html5,phaser.io,rts,starcraft,warcraft,canvas
description: "RPS: A Phaser.io Simple RTS game on HTML5"
author: Javier Rengel
featured: false
published: true
---

If you follow [me](http://twitter.com/rephus) or [Coconauts](http://twitter.com/coconauts) on Twitter, you might have seen some
GIFs about an RTS game we're developing on Phaser.io.

Codename: RPS (Rock, Paper, Scissors). Is a simple RTS (Real Strategy Game)
we're building using the Phaser.io framework for HTML5 games.

We aim to have a fun and complete game, with campaign, multiplayer
and with a fresh interface, compatible with mobile and touch devices.

The main selling point are its simple but effective mechanics.
Forget about hundreds of different units with their own powerups, multiple buildings,
or advanced research.

<!--more-->

This is some of the recent progress we've made in the last 2-3 weeks.

* Minimap
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Added a Minimap (bottom-rigth) to our game. It was easier than I expected <a href="https://twitter.com/hashtag/gamedev?src=hash">#gamedev</a> <a href="https://twitter.com/hashtag/phaserIo?src=hash">#phaserIo</a> <a href="https://t.co/A0ATvb5ZxM">pic.twitter.com/A0ATvb5ZxM</a></p>&mdash; Coconauts (@coconauts) <a href="https://twitter.com/coconauts/status/826728173996015616">February 1, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

* Simple AI, sight area, and new map
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Big progress on our RTS game in <a href="https://twitter.com/hashtag/phaserio?src=hash">#phaserio</a>: simple AI, sight area, new map and more! <a href="https://twitter.com/hashtag/gamedev?src=hash">#gamedev</a> <a href="https://t.co/jnxwofvm5c">pic.twitter.com/jnxwofvm5c</a></p>&mdash; Coconauts (@coconauts) <a href="https://twitter.com/coconauts/status/826161939478872069">January 30, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

* Emojis dialogs
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Express yourself with in-game emojis <a href="https://twitter.com/hashtag/gamedev?src=hash">#gamedev</a> <a href="https://t.co/l6RoYpmrrm">pic.twitter.com/l6RoYpmrrm</a></p>&mdash; Javi Rengel (@rephus) <a href="https://twitter.com/rephus/status/825682597170786304">January 29, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

If you want to follow these updates closely, checkout our [Project page](/projects/rps)

And let us know if you want to give us feedback about how to improve the game !
