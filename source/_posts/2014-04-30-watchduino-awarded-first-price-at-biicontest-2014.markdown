---
layout: post
title: "WatchDuino awarded first prize at Biicontest 2014!"
date: 2014-04-30 20:57:38 +0100
comments: true
categories: hardware
tags: watchduino
sharing: true
keywords: arduino,watchduino,smartwatch,contest,biicode,2014
description: "Watchduino: Smartwatch won first price in arduino contest 2014"
---

We can’t contain our excitement!

{% img  /images/posts/dalekparty.gif 350 350 %}

<!--more-->

You can check the lovely page that they made for our project [here](https://www.biicode.com/biicontest2014_first_winner#.U2FG93Wx3YA), 
and the full list of awarded projects [here](http://www.biicode.com/biicontest2014#.U2FMKXWx3YA) (we like them all!).
We put a lot of effort into neatly integrating WatchDuino with Biicode and creating some very friendly documentation, 
so we encourage you to give a try to [rolling out your own WatchDuino](http://bitbucket.org/rephus/watchduino/src/master/docs/how_to_replicate.md) 
and [learn a useful dependency management tool/platform](http://docs.biicode.com/) at the same time =)

We will invest part of the prize money into making donations to the open source projects powering WatchDuino, 
as well as to the author of the music we used in the video. We will probably buy us a cool treat as well, a 3D printer perhaps?