---
layout: post
title: "Spaceroads"
date: 2017-07-25 18:56:08 +0100
comments: true
categories: games
tags: Unity3D retro remake
sharing: true
keywords: skyroads,spaceroads,Unity3D
description: "We are working on a remake of Skyroads with Unity3D"
author: Mar Bartolome
featured: false
published: true
---

We are working on a remake of the classic
[Skyroads](http://www.bluemoon.ee/history/skyroads/) PC game.
The project is still in early stages of development but
[as promised](http://coconauts.net/blog/2016/01/01/year-retrospective/)
we want to give more continuous updates over the course of the development.

![](https://farm9.staticflickr.com/8420/29323471790_446a7fbc1b_z_d.jpg)

<!--more-->

## Skyroads

We are big fans of the [Skyroads game](http://www.bluemoon.ee/history/skyroads/)
by Bluemoon Interactive. Released on 1993, if you were a kid with a computer
during the 90s very likely you have come accross it in many of the popular
shareware compilations that roamed the world at the time.

In case you have never seen it, here's a sample:

<iframe width="560" height="315" src="https://www.youtube.com/embed/F6Rovi9QSDk" frameborder="0" allowfullscreen></iframe>

The game not only looks and sounds gorgeous, it also has a very neat level
design and a great difficulty curve that makes it incredibly addictive.
The levels not only play with the geometry of blocks, but also with parameters
such as the gravity (different on every planet!) and the oxigen and fuel gauges,
which add a lot of variety. For instance, in some levels you will start
with little oxygen, which will force you to run faster or you'll run out!
Whereas some longer levels will require you to ratio your resources better.

On top of all, this little gem was developed in just three months! Although
it benefited from being an iteration over a previous game the company had made,
[Kosmonaut](http://www.bluemoon.ee/history/kosmonaut/index.html).

## A remake

Why bother with a remake? There are already
[plenty of those](http://www.tastystatic.com/index.php)!
Even [open source ones](http://www.openroadsgame.com/).

We know. Our purpose with this project though is not so much to fill a gap
but to use it as a game development learning tool. The technical difficulties
of this game are not very big, yet still not trivial either. We expect it
shouldn't be too difficult to get a working engine, so we could then focus
on polishing the look and feel details and content, which is something we
rarely do, and will also be good practice.

We will initially target Spaceroads to be a desktop game, but if we reach to
a point we we are satisfied with the product we will see how we could adapt
the controls for mobile (this was our initial aim actually!) or even VR!

Fun fact: the developers of the original Skyroads game did in fact start
working on [a VR version](http://www.bluemoon.ee/history/stellar/index.html)
as far back as in 1994! Amazing.


## The (space)roadmap

Here are just some ideas of where this project could go. Some will happen,
some will not, or not in order:

- A game engine that looks and feels (and plays) like the original game
- A level editor
- Add polish and fluff (will not look like the original anymore)
- Server side, user generated content
- Campaign mode with a level progression and ... a story even?
- Port to mobile
- Port to VR

Updates will follow during our jorney, including code and playable versions.
For now we are still midway though step one, working on that game engine
with Unity3D. In case you are wondering, it's already looking like this!
