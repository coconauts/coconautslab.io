---
layout: page
title: "Water rings"
comments: false
sidebar: true
sharing: true
footer: true
---


Small Android game that simulates the classical water toy with colourful loops. The game mechanics is based on the insertion of all loops in the spikes, using the two button-simulating zones in the screen. When pushed, these buttons produce a torrent of bubbles, that lifts the loops upwards.

<img src="/projects/water-rings/toy.jpg" class="screenshot" />

It’s implementation included the simulation of realistic physics in order to simulate the aquatic movement of loops and bubbles, as well as a small particle engine to generate these.

<img src="/projects/water-rings/game1.png" class="screenshot" />

By using the options menu, the user can regulate the amount of air released when pushing the buttons, it’s sound, or the number of loops.

<img src="/projects/water-rings/options.png" class="screenshot" />

Links

The application is free to download from Google’s [Play Store](https://play.google.com/store/apps/details?id=net.coconauts.waterrings&feature=search_result#?t=W251bGwsMSwyLDEsIm5ldC5jb2NvbmF1dHMud2F0ZXJyaW5ncyJd)