---
layout: page
title: "Dynamus"
comments: false
sidebar: true
sharing: true
footer: true
---

Dynamus is a system for creating dynamic music engines, as well as a programming language for creating the logic that such engines have to follow.

By the term "dynamic music" we refer to music that is interactive, adaptive, or simply that is not static, and is generated dynamically following a set of rules. 

A Dynamus engine can be programmed in plain Java, or in the more declarative Dynamus programming language. An engine runs as a standalone entity, an is capable of receiving TCP events and responding to them according to it's rules, thus varying the music. The events can be sent asynchronously by any number of external client applications.

Dynamus' motivation is to simplify programming music for videogames (but can be applied to other interactive media as well). By both deocupling the music logic from the rest of the codebase, and providing a language more close to the domain if musicians, we tackle two of the main reasons why we are not seeing so much dynamic music in videogames nowdays.

Dynamus was Mar's thesis project for her bachelor's degree at university. It is currently discontinued, but the code and thesis document are public for anyone to browse. It's a project that Mar is particularly fond of, and her intention is to reworkit again someday. 

# Links

- [Source code](https://gitorious.org/dynamus)
- [Thesis document (in Spanish)](http://www.coconauts.net/mar/dynamus_thesis.pdf)
