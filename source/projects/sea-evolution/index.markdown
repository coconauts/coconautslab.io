---
layout: page
title: "Sea evolution"
comments: false
sidebar: true
sharing: true
footer: true
---

## Introduction

Sea evolution is a game developed for the Ludum Dare] 48h competition. Was developed under the last 12h in HTML5 and JavaScript.

<img src="/projects/sea-evolution/game1.png" class="screenshot" />

## Ludum Dare description

A fascinating world of creatures in the sea that need to eat and evolve to become stronger. All the creatures can eat or be eaten. If any creature can’t eat he dies after a time.

<img src="/projects/sea-evolution/game2.png" class="screenshot" />

You control one of the creatures while the others eat your food or try to eat you. The circle around you represents your hunger, if you can’t eat in a time you’ll die like the others and the game is over. Try to stay alive all the time you can.

There are three levels of evolution and the last of them can lay eggs to make life go on. Each creature has special abilities that make them stronger to another species (attack, defense, speed and others).

<img src="/projects/sea-evolution/evolution_tree.png" class="screenshot" />

Also, you can play or see how the evolution happens in the sea.

## Links

* [Sea Evolution game](http://html5.coconauts.net/evolution5/) 
* [Ludum Dare post](http://www.ludumdare.com/compo/ludum-dare-24/?action=preview&uid=16605)
