---
layout: page
title: "404 games"
comments: false
sidebar: true
sharing: true
footer: true
categories: games
tags: games 404 classic html5 canvas pacman space-invaders
keywords: games,404,classic,html5,canvas,pacman,space-invaders,opensource
description: "Opensource classic HTML5 and Canvas 404 games like pacman or space-invaders"
---

Getting a [404](http://coconauts.net/404) page is not something enjoyable,
that's why we wanted to build something nice for anyone that lands in this
unexpected place.

So we built a few simple classic games in HTML5 + Canvas, and you will get randomly
one of these everytime you land in our [404](http://coconauts.net/404) page.

## Space invaders

<img src="/projects/404games/space-invaders.png" class="screenshot" />

[Test me](http://coconauts.net/html5/space-invaders)

## Pacman

<img src="/projects/404games/pacman.png" class="screenshot" />

[Test me](http://coconauts.net/html5/pacman)

## Snake

<img src="/projects/404games/snake.png" class="screenshot" />

[Test me](http://coconauts.net/html5/snake)

Fortunately, 404 pages don't happen too often in [Coconauts](http://coconauts.net),
but still, this is an improvement over the previous version,
and we had fun building this.

## Follow up

- Github code repository: https://github.com/coconauts/404-games
