---
layout: page
title: "Spaceroads"
comments: true
sidebar: true
sharing: true
footer: true
---


## Skyroads

We are big fans of the [Skyroads game](http://www.bluemoon.ee/history/skyroads/)
by Bluemoon Interactive. Released on 1993, if you were a kid with a computer
during the 90s very likely you have come accross it in many of the popular
shareware compilations that roamed the world at the time.

You can play our demo directly on your [browser](http://unity3d.coconauts.net/spaceroads-v10/)

![gameplay](/images/posts/2017-11-06-spaceroads-demo/v10-gameplay.gif)


The game not only looks and sounds gorgeous, it also has a very neat level
design and a great difficulty curve that makes it incredibly addictive.
The levels not only play with the geometry of blocks, but also with parameters
such as the gravity (different on every planet!) and the oxigen and fuel gauges,
which add a lot of variety. For instance, in some levels you will start
with little oxygen, which will force you to run faster or you'll run out!
Whereas some longer levels will require you to ratio your resources better.

On top of all, this little gem was developed in just three months! Although
it benefited from being an iteration over a previous game the company had made,
[Kosmonaut](http://www.bluemoon.ee/history/kosmonaut/index.html).

## A remake

Why bother with a remake? There are already
[plenty of those](http://www.tastystatic.com/index.php)!
Even [open source ones](http://www.openroadsgame.com/).

We know. Our purpose with this project though is not so much to fill a gap
but to use it as a game development learning tool. The technical difficulties
of this game are not very big, yet still not trivial either. We expect it
shouldn't be too difficult to get a working engine, so we could then focus
on polishing the look and feel details and content, which is something we
rarely do, and will also be good practice.

We will initially target Spaceroads to be a desktop game, but if we reach to
a point we we are satisfied with the product we will see how we could adapt
the controls for mobile (this was our initial aim actually!) or even VR!

Fun fact: the developers of the original Skyroads game did in fact start
working on [a VR version](http://www.bluemoon.ee/history/stellar/index.html)
as far back as in 1994! Amazing.


## Features

### Three adventure levels

We built the first 3 level of the original Skyroads in our editor, with the same layout, blocks and feels (the color might be different).

![gameplay](/images/posts/2017-11-06-spaceroads-demo/v10-gameplay.png)


## Level editor

We built a fully featured 3D editor for the game. You can add different block types, change start point, undo, preview levels or  publish them online. 

![gameplay](/images/posts/2017-11-06-spaceroads-demo/v10-editor.gif)

It has everything you need to build levels like in the original skyroads.

![editor](/images/posts/2017-11-06-spaceroads-demo/v10-editor.png)

You can even add effects (burning floor, recharge, speed up/down, etc)

![effects](/images/posts/2017-11-06-spaceroads-demo/v10-editor-effects.png)

Or color to the blocks

![color](/images/posts/2017-11-06-spaceroads-demo/v10-color.png)


Note: Saving levels or publishing online is built
but not supported on the demo version 


## Multiverse

Multiverse is not available on the demo, but this feature will allow players to list their built levels, and levels available online.

![multiverse](/images/posts/2017-11-06-spaceroads-demo/v10-multiverse.png)


[This version](http://unity3d.coconauts.net/spaceroads-v10/)
 is not complete and it can be a bit buggy, but we want you to test it and give us feedback before we continue with its development. So let us know what you think in the comments.

 ![spaceroads](/images/posts/2017-11-06-spaceroads-demo/v10-main-menu.png)
