---
layout: page
title: "Water rings 2"
comments: false
sidebar: true
sharing: true
footer: true
---

A few years ago we released our very first Android Game (written on pure Android code and not using LibGdx) 
called [Water Rings](https://play.google.com/store/apps/details?id=net.coconauts.waterrings)

But recently we released an improved and more accurate version of this classic game using Unity3D and its physic engine.

You can download the game for free on [Google Play](https://play.google.com/store/apps/details?id=net.coconauts.water3d)

The game also supports Accelerometer, and you can make  your screen transparent so it seems like you are holding a real
water ring toy.

Also, don't miss the teaser trailer

<iframe width="100%" height="500" src="https://www.youtube.com/embed/5VBlgFSvP_w" frameborder="0" allowfullscreen></iframe>