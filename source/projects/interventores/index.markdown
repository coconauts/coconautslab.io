---
layout: page
title: "Interventores"
comments: false
sidebar: true
sharing: true
footer: true
---

Interventores.info is a discussion forum commissioned by the group of regional inspectors of the Málaga Local Administration.

<img src="/projects/interventores/interventores.png" class="screenshot" />

## Technologies

* PHP5
* MySQL
* CSS
* phpBB
