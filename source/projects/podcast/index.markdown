---
layout: page
title: "Podcast (en español)"
comments: true
sidebar: true
sharing: true
footer: true
---

Coconauts es un podcast sobre tecnologia, desarrollo software, gamedev y making.

* Canal en ivoox: http://gb.ivoox.com/es/podcast-coconauts_sq_f1385224_1.html
* Canal en itunes: https://itunes.apple.com/us/podcast/id1203811296
* Ask.fm para preguntitas https://ask.fm/coconauts

## Suscribirse

<iframe id='button_subscribe_ivoox385224' src='http://www.ivoox.com/_p3_385224_1.html?c1=ff6600&c2=ffffff&c3=000000' frameborder='0' allowfullscreen='' scrolling='no' height='95' style='max-size:300px;' ></iframe>

## Capítulos

### Piloto 

<iframe id='audio_16881889' frameborder='0' allowfullscreen='' scrolling='no' height='200' style='border:1px solid #EEE; box-sizing:border-box; width:100%;' src="https://gb.ivoox.com/es/player_ej_16881889_4_1.html?c1=ff6600"></iframe>

En este episodio hablamos de:

- Noticias: Zenimax vs Oculus, Asus Tinkerboard, Libretaxi y cancelacion de Scalebound.
- Yo he venido aqui a hablar de mi libro: Masters of Doom
- Debate: creatividad computacional

Enlaces relacionados:

- [Oculus vs Zenimax](http://www.polygon.com/2017/2/1/14474198/oculus-lawsuit-verdict)
- [Asus Tinkerboard](https://www.engadget.com/2017/01/23/asus-tinker-board/)
- [Libretaxi (making of)](https://medium.com/@romanpushkin/how-i-made-uber-like-app-in-no-time-with-javascript-and-secret-sauce-94ef9120c7f6#.cc6jss25p)
- [Desarrolladores de scalebound sobre su cancelacion](http://uk.ign.com/articles/2017/01/11/scalebound-cancellation-developers-comment-on-the-situation)
- [Masters of Doom](https://www.amazon.es/Masters-Doom-Created-Transformed-Culture/dp/0749924896/ref=sr_1_1?ie=UTF8&qid=1486501773&sr=8-1&keywords=masters+of+doom)
- [Jukedeck @ London Techcrunch Disrupt 2015](https://www.youtube.com/watch?v=YY2FPWWc_Sk)

### S1E1 Psicologia objetos cotidianos y game frameworks

<iframe id='audio_17098008' frameborder='0' allowfullscreen='' scrolling='no' height='200' style='border:1px solid #EEE; box-sizing:border-box; width:100%;' src="https://www.ivoox.com/player_ej_17098008_4_1.html?c1=ff6600"></iframe>

En este episodio hablamos de:

- Noticias: el outage de Gitlab, Steam cierra Greenlight, RethinkDB y Cloud Spanner, Gameband.
- Yo he venido aquí a hablar de mi libro: la psicología de los objetos cotidianos de Don Norman
- Debate: frameworks de desarrollo de videojuegos

Enlaces relacionados:

- [Gitlab outage postmortem](https://about.gitlab.com/2017/02/10/postmortem-of-database-outage-of-january-31/)
- [Steam greenlight](http://steamcommunity.com/greenlight/discussions/18446744073709551615/133256758580075301/)
- [RethinkDB](https://rethinkdb.com/blog/rethinkdb-joins-linux-foundation/)
- [Google Cloud Spanner](https://cloudplatform.googleblog.com/2017/02/introducing-Cloud-Spanner-a-global-database-service-for-mission-critical-applications.html)
- [Gameband](https://www.kickstarter.com/projects/gameband/gameband-the-first-smartwatch-for-gamers?ref=category_newest)
- [Watchduino](https://www.youtube.com/watch?v=CtgR1YiwnEY)
- [La psicologia de los objetos cotidianos](https://www.amazon.es/psicolog%C3%ADa-objetos-cotidianos-Serie-Media/dp/8415042019)
- [Comparativa de game frameworks](http://coconauts.net/blog/2017/01/09/2d-game-framework-comparison/)

### S1E2 Docker y Encuesta Stack Overflow

<iframe id='audio_18055515' frameborder='0' allowfullscreen='' scrolling='no' height='200' style='border:1px solid #EEE; box-sizing:border-box; width:100%;' src="https://gb.ivoox.com/es/player_ej_18055515_4_1.html?c1=ff6600"></iframe>

En este episodio hablamos de:

* Noticias: scala native, Google Cloud Next
* Introducción a docker
* Encuesta de desarrolladores de Stack Overflow

Enlaces relacionados:

- [Gitlab outage postmortem](https://about.gitlab.com/2017/02/10/postmortem-of-database-outage-of-january-31/)
- [Steam greenlight](http://steamcommunity.com/greenlight/discussions/18446744073709551615/133256758580075301/)
- [RethinkDB](https://rethinkdb.com/blog/rethinkdb-joins-linux-foundation/)
- [Google Cloud Spanner](https://cloudplatform.googleblog.com/2017/02/introducing-Cloud-Spanner-a-global-database-service-for-mission-critical-applications.html)
- [Gameband](https://www.kickstarter.com/projects/gameband/gameband-the-first-smartwatch-for-gamers?ref=category_newest)
- [Watchduino](https://www.youtube.com/watch?v=CtgR1YiwnEY)
- [La psicologia de los objetos cotidianos](https://www.amazon.es/psicolog%C3%ADa-objetos-cotidianos-Serie-Media/dp/8415042019)
- [Comparativa de game frameworks](http://coconauts.net/blog/2017/01/09/2d-game-framework-comparison/)
