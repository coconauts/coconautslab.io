---
layout: page
title: "Bookmark manager"
comments: false
sidebar: true
sharing: true
footer: true
---

## Introduction

This plugin for Google Chrome provides a quick and easy way to access and search your bookmarks in the popular browser, adding new functionality.

This is a work in progress, some of the features you see, may not be available in the current version.

<img src="/projects/bookmark/bookmarks1.png" class="screenshot" />

## Features

* List all google chrome bookmarks
* Search dialog
* Tag system
* Detect broken /old bookmarks (503, 400)
* Detect duplicated bookmarks
* Usual bookmarks actions (add, remove, open) allows you to replace the default google chrome bar completely.
* Take the current tab when adding a new bookmark.

<img src="/projects/bookmark/bookmarks2.png" class="screenshot" />

## Tag system

This plugin allows you to add tags to every bookmark using some special tags in the bookmark name. Then you can search by tag or see all tagged bookmarks in the second tab.

<img src="/projects/bookmark/bookmarks_add.png" class="screenshot" />
 
## Links

This is still a work in progress, soon we will share all links with you.