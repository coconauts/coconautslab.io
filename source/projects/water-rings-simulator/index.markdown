---
layout: page
title: "Water Rings Simulator"
comments: false
sidebar: true
sharing: true
footer: true
---

Water Rings Simulator is the new version of the classic toy game on Unity3d exlusive for Android. 

<iframe width="560" height="560" src="https://www.youtube.com/embed/5VBlgFSvP_w" frameborder="0" allowfullscreen></iframe>

This game features: 

- Realisitc underwater physics
- 3D graphics
- Mobile movement detection
- Realistic visual effects
- Transparent phone background (using camera to see behind it)
- It's FREE!

![game screenshot](/images/posts/2017-04-20-water-rings-simulator/transparent.png)

Download the game for free on the [Android Play Store](https://play.google.com/store/apps/details?id=net.coconauts.water3d)