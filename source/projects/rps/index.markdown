---
layout: page
title: "RPS: a Phaser.io simple RTS"
comments: true
sidebar: true
sharing: true
footer: true
---

Codename: RPS (Rock, Paper, Scissors). Is a simple RTS (Real Strategy Game)
we're building using the Phaser.io framework for HTML5 games.

This is a work in progress.

We aim to have a fun and complete game, with campaign, multiplayer
and with a fresh interface, compatible with mobile and touch devices.

The main strong point are its simple but effective mechanics.
Forget about hundreds of different units with their own powerups, multiple buildings,
or advance research.

You can still do zergling rush though.

## Progress

This is the progress we have done so far (newer to older)

* Minimap
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Added a Minimap (bottom-rigth) to our game. It was easier than I expected <a href="https://twitter.com/hashtag/gamedev?src=hash">#gamedev</a> <a href="https://twitter.com/hashtag/phaserIo?src=hash">#phaserIo</a> <a href="https://t.co/A0ATvb5ZxM">pic.twitter.com/A0ATvb5ZxM</a></p>&mdash; Coconauts (@coconauts) <a href="https://twitter.com/coconauts/status/826728173996015616">February 1, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

* Simple AI, sight area, and new map
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Big progress on our RTS game in <a href="https://twitter.com/hashtag/phaserio?src=hash">#phaserio</a>: simple AI, sight area, new map and more! <a href="https://twitter.com/hashtag/gamedev?src=hash">#gamedev</a> <a href="https://t.co/jnxwofvm5c">pic.twitter.com/jnxwofvm5c</a></p>&mdash; Coconauts (@coconauts) <a href="https://twitter.com/coconauts/status/826161939478872069">January 30, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

* Emojis dialogs
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Express yourself with in-game emojis <a href="https://twitter.com/hashtag/gamedev?src=hash">#gamedev</a> <a href="https://t.co/l6RoYpmrrm">pic.twitter.com/l6RoYpmrrm</a></p>&mdash; Javi Rengel (@rephus) <a href="https://twitter.com/rephus/status/825682597170786304">January 29, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

* Smart unit positioning
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">I forgot to show the smart positioning for my RTS game using easypath for Phaser.io <a href="https://twitter.com/hashtag/gamedev?src=hash">#gamedev</a> <a href="https://twitter.com/hashtag/screenshotSaturday?src=hash">#screenshotSaturday</a> <a href="https://t.co/DY5NBstLL1">pic.twitter.com/DY5NBstLL1</a></p>&mdash; Javi Rengel (@rephus) <a href="https://twitter.com/rephus/status/825406829882699778">January 28, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

* Create buildings
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">New RTS gameplay: units create buildings and buildings create units. <a href="https://t.co/OEKpkpTZVy">pic.twitter.com/OEKpkpTZVy</a></p>&mdash; Javi Rengel (@rephus) <a href="https://twitter.com/rephus/status/824744287011209216">January 26, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

* Collect resources
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">More RTS gameplay: collecting resources and selecting units on Phaser.io <a href="https://twitter.com/hashtag/gamedev?src=hash">#gamedev</a> <a href="https://t.co/IO5S69WarV">pic.twitter.com/IO5S69WarV</a></p>&mdash; Javi Rengel (@rephus) <a href="https://twitter.com/rephus/status/823274823044435968">January 22, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

* Pathfinding easypath.js
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Testing pathfinding, animations and AI on phaser.io <a href="https://twitter.com/hashtag/gamedev?src=hash">#gamedev</a> <a href="https://t.co/6xxVGkBAJa">pic.twitter.com/6xxVGkBAJa</a></p>&mdash; Javi Rengel (@rephus) <a href="https://twitter.com/rephus/status/823176974218756098">January 22, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
