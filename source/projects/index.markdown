---
layout: page
title: "Projects"
description: "All Coconauts projects"
comments: false
sidebar: false
sharing: true
footer: true
---
<link href="/stylesheets/projects.css" rel="stylesheet" type="text/css">
<script languaje='javascript' src="/javascripts/projects.js"></script>

<h3>All projects</h3>
<table id="projects"></table>

<h3>Projects by Year</h3>
<div id="projectYear"></div>

<h3>Projects by Type</h3>
<div id="projectStatus"></div>

<script language="javascript">

add("spaceroads", 2017, "games",
    "SpaceRoads",
    "Skyroads remake on Unity3D",
    "Unity3D");

add("escaperoom", 2017, "games",
    "Escaperoom",
    "Generative puzzle videogame",
    "Unity3D");

add("infinify", 2017, "software",
    "Infinify",
    "Infinite recommender for Spotify",
    "NodeJS, Spotify API");

add("github-pr-tracker", 2017, "software",
    "Github PR tracker",
    "Podcast about games, hardware and stuff (in spanish)",
    "Audacity, Ivoox, Itunes");

add("podcast", 2017, "general",
    "Podcast",
    "Podcast about games, hardware and stuff (in spanish)",
    "Audacity, Ivoox, Itunes");

add("rps", 2017, "games",
    "RPS",
    "A Phaser.io Simple RTS",
    "Javascript, Phaser.io");

add("hardware", 2017, "hardware",
    "Small hardware projects",
    "Compilation of all our small hardware projects",
    "C++, Arduino, Electronics, Raspberry PI");

add("retrophies", 2016, "games",
    "Retrophies",
    "Achievements system for Emulators",
    "Javascript");

add("3d-house", 2016, "games",
    "3D house",
    "3D playable model of our house",
    "Unity3D, Blender");

add("starcraft-unity3d", 2016, "games",
    "StarCraft Unity3D",
    "3D playable model of our house",
    "Unity3D, Blender");

add("coconauts-ci", 2016, "software",
    "Coconauts CI",
    "Lightweight CI app",
    "NodeJS, Redis");

add("water-rings-simulator", 2015, "games",
    "Water Rings Simulator",
    "New version of Water rings toy simulator on Unity3D for Android",
    "Unity3D");

add("sea-life", 2015, "games",
    "Sea-Life",
    "Procedurally generated multiplayer game powered by Websockets for #procjam2015",
    "HTML5, Javascript, Websockets");

add("404games", 2015, "games",
    "404 games",
    "Collection of small HTML5 games for our 404 page",
    "HTML5, Javascript");

add("watchduino2", 2015, "hardware",
    "WatchDuino 2.0",
    "WatchDuino 2.0 is a SmartWatch built in Arduino that uses Bluetooth for sending messages to Android",
    "C++, Arduino, Electronics, Android, Java");

add("gramola", 2015, "software",
    "Gramola",
    "Lightweight online music player, powered by JPlayer with tons of features",
    "Javascript, SqLite, NodeJS, Css3");

add("water-rings2", 2015, "games",
    "Water Rings 2",
    "Enhaced version of our water rings toy simulator",
    "Unity3D, C#");

add("3d-island-demo", 2015, "games",
    "3D Island Demo",
    "Virtual reality game demo for Android built with Unity3D",
    "Unity3D, C#");

add("watchduino", 2014, "hardware",
    "WatchDuino",
    "WatchDuino is an open hardware project that combines inexpensive electronic components and a complex Arduino (C++) code to build a useful and reprogrammable smart watch",
    "C++, Arduino, Electronics");

add("timebomb2", 2014, "games",
    "Exploding bunnies",
    "Sequel of Time Bomb for Android, with a lot of new content, achievements, unlockable items, etc",
    "Java, Android, LibGdx");

add("game-manager", 2014, "software",
    "Game manager",
    "App built with nodejs for managing local game repositories and emulators",
    "NodeJs, SqLite");

add("astroship", 2013, "games",
    "Astroship",
    "Our first mobile game powered with LibGdx, and with achievements",
    "Java, Android, LibGdx");

add("cleope", 2013, "software",
    "CleopeAlhaurin.es",
    "Online store for a local business (commissioned)",
    "Magento, PHP, Css3");

add("interventores", 2013, "software",
    "Interventores.info",
    "Collaborative forum for regional inspectors (commissioned)",
    "phpBB, PHP, Css3");

add("timebomb", 2013, "games",
    "TimeBomb",
    "Game developed in less than 48 hours for Ludum Dare #27",
    "Java, Android, LibGdx");

add("gramola", 2012, "software",
    "Rephusplayer",
    "Lightweight online music player initially built in PHP, then translated to NodeJS and renamed as Gramola",
    "Javascript, SqLite, PHP, Css3");

add("sea-evolution", 2012, "games",
    "Sea Evolution",
    "Game developed for the Ludum Dare #24 competition ",
    "Javascript, HTML5");

add("piano-legend", 2012, "games",
    "Piano Legend",
    "Piano simulator game",
    "Java, SDL");

add("dynamus", 2012, "software",
    "Dynamus",
    "Dynamic music engine and domain specific language",
    "Java, Lex+Yacc");

add("water-rings", 2011, "games",
    "Water Rings",
    "Water rings toy simulator",
    "Java, Android");


/*
HIDDEN

add("turret-pi", 2014, progress,
    "Turret Pi",
    "Recreation of a Portal turret with a Raspberry Pi",
    "Python, Raspberry Pi, Electronics");

add("spend", 2014, progress,
    "Spend manager",
    "Program to manage and visualize your expenses",
    "Scala, SqLite, Javascript");

add("bookmark", 2013, progress,
    "Bookmarks",
    "Advanced bookmark manager for Google Chrome",
    "Javascript, Css3");  


    */
</script>
