---
layout: page
title: "Sea-Life: Game for ProcJam 2015"
comments: false
sidebar: true
sharing: true
footer: true
---

Play the game [here](http://html5.coconauts.net/sealife/) or check the [Github repo](https://github.com/coconauts/sea-life)

![](https://pbs.twimg.com/media/CTe-hdHWsAAEDQy.png)

## Description

This game attempted to be an iteration on a similar idea that we did for a Ludum Dare, ["Sea Evolution"](http://ludumdare.com/compo/ludum-dare-24/?action=preview&uid=16605)

In the game you are sea creature that needs to survive in your ecosystem. You get hungry, you need to eat, and you can lay eggs to perpetuate your species. Different sea creatures have different attributes, which determine who can eat and get eaten by.

Unfortunately we didn't have a lot of time during this week, so we couldn't develop or polish as much as we wanted! Here's what we managed to do:

## Infinite game world

In the original sea evolution there was no scrolling. But we wanted to make it feel like the endless sea, so in this one we made a scrolling world that gets autopopulated with new fishes as you advance through it.

Our original plan was to emulate a sea bottom, which we would also autopopulate with seaweeds, reefs, or even dark abyss areas, or polluted areas where only poison-resistant fishes would be able to habitate. But we didn't have time for all this! So instead you are just traversing the boring open ocean.

## 100% canvas, randomized fish shapes

Everything is drawn with canvas, no sprites! It's the first time we actually do this sort of thing, and it was challenging but quite fun. This allows for some fun features, such as being able to alter the shape of the fishes programatically. Each fish will randomly be given a distinct color and shape.

## Autogenerated fish statistics

Fishes are created with a set of stats: speed, attack power and defense. These are represented by the gauge meters below the fish. Normally you should be safe to attack a fish if it has less attack and defense than you do (otherwise the other fish might kill you!). The stats are balanced, so that if you get more of one you'll get less of the others.

## Fish growth

In the original Sea Evolution, fishes went through several life stages, from egg to adult. On each life stage their stats were bumped. You were always able to survive by eating the fishes that were in lower life stages, beacause they had lower stats. On adult form, fishes can lay eggs to perpetuate there species.

In this game we have implemented the life stages, and you can notice that after a certain amount of time the stats are bumped. However we didn't have timeto alter the appearance of the fishes when this happens.

## Controls

You can move with the arrow keys or the mouse. The enemy fishes randomly pick where to go, or if they are close to other fish they will chase it to eat it. Sometimes this results in all of the enemies gathering together in one point eating each other out.

The player fish also follows the same behavior if you don't tell it where to go. This is a bit confusing, yes, but we didn't have time to change it.

## Descendants

The original idea for Sea Life was to be able to perpetuate your species over the rest. You would be able to lay eggs and have descendancy, and your success in life (ie, your game score) would be measured to how many descendants did you managed to get until you died. How successful you would be in your species conquering the sea world would depend in both your ability and the statistics autogenerated for your sea species.

So we did implement this... partially. You will have children from time to time, and they will follow you along. But actually they don't really have any stats, or count as real fishes. But they make you feel less lonely, no?

## Websockets

Yay! Another experiment. We wanted to make this a multiplayer experience, using websockets. When you join the game you will connect to a server keeping track of all the players, and you will receive their possitions. So you might cross another human while playing!

It's easy to identify a human: they will have no stats displaying (because you can't really eat them), and they would have an autogenerated name, in the pattern "adjective + sea animal" (in the spirit of Metal Gear Solid troop names, or Ubuntu distributions)

And that is basically it. Probably too buggy to be playable for a long time, but a fun experiment with some forms of procedural generation =)
