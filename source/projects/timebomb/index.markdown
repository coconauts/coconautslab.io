---
layout: page
title: "Timebomb"
comments: false
sidebar: true
sharing: true
footer: true
---

## Introduction

This game was developed in less than 48 hours for Ludum Dare #27. A big wave of 10 seconds time bombs are going to kill that cute rabbit, you must save him.

<img src="/projects/timebomb/menu.png" class="screenshot" />

## Story

Lollipop is a small happy rabbit who lives in a field with his family. Suddenly, a war started in the area and two different opponents are fighting each other with timer bombs (or explosive pineapples) and it becomes harder and harder.
Unfortunately, Lollipop’s family died in the war, now he must survive to revenge his family (maybe some day he will kill the responsible of this war, who knows).

## Features

* The main player is a cute rabbit.
* Multiple SCOREBOARDS !!!!!

<img src="/projects/timebomb/scoreboard.png" class="screenshot" />

* Multiple game modes (10 seconds wave or continuous)
* Options menu (Try the explosive pineapples!)
* Music
* Android version

If you have a small screen resolution you can resize the screen, the aspect ratio is made for phones.

## How to play

Click at any place of the screen to guide Lollipop (the rabbit).
Stay away from the bombs.
Stay away from the borders of the screen (a bomb could suddenly appear).

<img src="/projects/timebomb/game1.png" class="screenshot" />

## Credits

* Developed using LibGdx,
* Most of the graphics are handmade using Gimp (bomb and pineapple sprite donwloaded from Openclipart)
* Sounds made with CFXR.
* The music has been generated with autotracker.

<img src="/projects/timebomb/game_over.png" class="screenshot" />

## Links

* [Ludum dare entry](http://www.ludumdare.com/compo/ludum-dare-27/comment-page-1/?action=preview&uid=16605)
* [Java Executables](https://bitbucket.org/rephus/timebomb/downloads)
* [Android version in Google Play](https://play.google.com/store/apps/details?id=net.coconauts.timebomb)

Want more ? See our sequel [Timebomb 2](/projects/timebomb2)
