---
layout: page
title: "Exploding bunnies"
comments: false
sidebar: true
sharing: true
footer: true
---

## Introduction

*Exploding bunnies* is an Android game, and a sequel to *Time Bomb*, a game Javier made for the [Ludum Dare](http://www.ludumdare.com) game jam. Being developed in 48 hours, while it was very fun, it was also very rough around the edges. We decided to take the gameplay and concept of the original game and enhance them, while providing more pleasing graphics and music, and *Exploding bunnies* was born.

<img src="/projects/timebomb2/main-super.png" class="screenshot" />
<img src="/projects/timebomb2/game.png" class="screenshot" />
<img src="/projects/timebomb2/game-over.png" class="screenshot" />
<img src="/projects/timebomb2/options.png" class="screenshot" />

*Exploding bunnies* is an old-school style arcade game where you are a very cute bunny rabbit that just happens to be in the middle of a bombing. Just your luck! Your task is to dodge bombs and keep alive, for as long as you can! Simple mechanics, yet high addiction factor.

*Exploding bunnies* spices up this concept over the original one by including several gameplay extras: different types of bombs, with different dodging strategy (watch out for those pesky underground mines!), and little rabbits to rescue, which you can exchange for power ups and lovely ornamental accessories.

<img src="/projects/timebomb2/power-ups.png" class="screenshot" />
<img src="/projects/timebomb2/pimps.png" class="screenshot" />

*Exploding bunnies* features 8-bit style graphics and music that suit up perfectly the simple mechanics of the game. It’s also fully integrated with Google Play Services leaderboards and achievements.

It’s fully free to play and completely microtransaction free. You can though, if you wish, make a small donation through the game itself, and who knows, something good might happen if you do!

<img src="/projects/timebomb2/about.png" class="screenshot" />

As for the geeky details, *Exploding bunnies* was developed, as the original one, using the [LibGDX](http://libgdx.badlogicgames.com) game framework. Graphics were made with the [Gimp](http://www.gimp.org) image editor, and music was made using [LMMS](http://lmms.sourceforge.net) and the [Peach](http://www.tweakbench.com/peach) and [Toad](http://www.tweakbench.com/toad) VSTs, and has a small level of [dynamicity](http://en.wikipedia.org/wiki/Dynamic_music) implemented. It was thoroughly tested on a Nexus One and a Nexus 4 phones, and no bunnies were harmed during the development of the game. 

*Exploding bunnies* can be downloaded from the [Google Play Store](https://play.google.com/store/apps/details?id=net.coconauts.timebomb2). The original *Time Bomb*, by the way, is also available in [the Play Store](https://play.google.com/store/apps/details?id=net.coconauts.timebomb), as well as in [the original Ludum Dare submission](http://www.ludumdare.com/compo/ludum-dare-27/?action=preview&uid=16605) that started it all.