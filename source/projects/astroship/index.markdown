---
layout: page
title: "Astroship"
comments: false
sidebar: true
sharing: true
footer: true
---

Astroship is a mobile game, and the first in our series of Reverse Classics, which explores reversed gameplays of classic videogames: you play the role of the enemy instead of the hero.

<img src="/projects/astroship/screen1.png" class="screenshot" />

In Astroship we reversed the classic Atari Asteroids™, your new mission being destroying that pesky spaceship by placing asteroids in the screen.

<img src="/projects/astroship/screen2.png" class="screenshot" />
<img src="/projects/astroship/screen3.png" class="screenshot" />

Three game modes are available:

* Arcade, tries to recreate the original arcade mode as faithfully as possible. For this, as in the classic Atari Asteroids, you will start with 4 asteroids, and the ship will pass to the next level if it destroy them all. As levels go by, you’ll be given more asteroids to attack the ship with, just like in the original game. You’ll have to destroy the ship three times to clear this mode.

* Time Attack, in which you’ll have to try to destroy the ship as quickly as possible. All the asteroids you use will be regenerated, so you have unlimited resources.

* UnSurvival, which like Arcade is divided in levels, but your mission is to not let the ship survive a single level! The ship will start rather clumsy at first, but will become a formidable enemy as you advance. How many levels can you hold?

Astroship implements a Google Play scores and achievements system, so you’ll be able to compete against all other Google Play users.

Download link will be available soon