---
layout: page
title: "Cleope"
comments: false
sidebar: true
sharing: true
footer: true
---

Cleope is a local Malagenian business offering cosmetic wellbeing products and treatments, which can be browsed and purchased as well via [cleopealhaurin.es](http://cleopealhaurin.es).

<img src="/projects/cleope/cleope.png" class="screenshot" />

The website was made from scratch to replace an older static HTML version of the same. It was required that it should retain the look and feel of the previous one, while allowing for an easier management of the products and content.

## Technologies

* PHP5
* Magento
* CSS
* MySQL
