---
layout: page
title: "Press"
comments: false
sidebar: true
sharing: false
footer: true
description: "Coconauts press center. All the articles talking about us"
---

Great articles talking about us!

### Retrophies

[Project](/projects/retrophies)

- [HackerNews (front page!)](https://news.ycombinator.com/item?id=12205560)
- [r/emulation](https://www.reddit.com/r/emulation/comments/4voi81/retrophies_achievements_system_for_emulators/)

### Lightning-dodge-O-matic

[Blog entry](/blog/2016/06/13/lightning-dodge-o-matic/)

* [Hackaday](http://hackaday.com/2016/06/22/cheating-at-video-games-arduino-edition/)
* [Custom Protocol](http://www.customprotocol.com/autres/vita-lightning-dodge-o-matic-final-fantasy-x-eclairs/)

### Watchduino 2

[Project](/projects/watchduino2)

- [Atmel](http://blog.atmel.com/2015/09/02/tell-time-and-more-on-this-open-source-bluetooth-enabled-watch/)
- [MakerNews](http://makernews.info/featured/2015/09/watchduino-2-an-arduino-based-smartwatch-that-doesnt-look-like-a-bomb.html)
- Semifinalist in the [Hackaday Prize 2015](http://hackaday.com/2015/08/24/100-semifinalists-for-the-2015-hackaday-prize/)

### Watchduino

[Project](/projects/watchduino)

- [Hackaday](http://hackaday.com/2014/05/06/tell-time-and-blink-an-led-on-your-wrist-with-watchduino/)
- [Atmel](http://blog.atmel.com/2014/05/08/atmega328-powers-open-source-watchduino/)
- [electronics-lab](http://www.electronics-lab.com/watchduino-arduino-watch/)
- Winner of the [Biicode contest 2014](http://blog.bricogeek.com/noticias/programacion/resultados-y-ganadores-de-biicode-contest-2014/)
- [Semageek (French)](http://www.semageek.com/watchduino-une-montre-open-hardware-sous-arduino/)
