<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
<title><![CDATA[Tag: html5 | Coconauts]]></title>
<link href="https://coconauts.net/blog/tags/html5/atom.xml" rel="self"/>
<link href="https://coconauts.net/"/>
<updated>2017-11-30T13:41:40+00:00</updated>
<id>https://coconauts.net/</id>
<author>
<name><![CDATA[Coconauts]]></name>
<email><![CDATA[admin@coconauts.net]]></email>
</author>
<generator uri="http://octopress.org/">Octopress</generator>

<entry>
<title type="html"><![CDATA[Procedurally generated engine with websockets]]></title>
<link href="https://coconauts.net/blog/2017/10/30/proc-websockets/"/>
<updated>2017-10-30T12:23:46+00:00</updated>
<id>https://coconauts.net/blog/2017/10/30/proc-websockets</id>
<content type="html"><![CDATA[<p>We have been working for some time on an engine for cocos2d for
persistent procedurally generated 2D worlds, for building a few ideas based on this concept, like a Zelda-like RPG multiplayer game.</p>

<p><img src="/images/posts/2017-10-30-proc-websockets/sector-cocos.png" alt="screenshot" /></p>

<p>The engine is built in NodeJS, and the server-client communication is fully implemented with websockets (using Socket.io), cocos2d in the frontend and built together with docker.</p>

<h2>Engine features</h2>

<ul>
<li>The client will only render the sectors around you, not the whole world.</li>
<li>When the player discovers a new sector, the server will populate that sector (new discovered sectors apperars brighter to the user)</li>
<li>The currenct sector will appear as red</li>
<li>Supports multiplayer (you can see other players next to you)</li>
<li>Random name generator for players</li>
<li>Move the player using the keyboard, the camera will follow.</li>
</ul>


<p><img src="/images/posts/2017-10-30-proc-websockets/sector-cocos-description.png" alt="description" /></p>

<h2>Try it out (docker)</h2>

<p>If you want to use it for your own game, You can find the code on the repo <a href="https://github.com/rephus/proc-cocos">https://github.com/rephus/proc-cocos</a> and share your creation in the comments.</p>

<p>so all you need to run it is to install docker and then <code>docker-compose up</code>.</p>

<p>Open the client on a browser in <a href="http://localhost:8000">http://localhost:8000</a></p>

<p>We may use this engine eventually to build our own game, stay tuned for more details.</p>
]]></content>
</entry>

<entry>
<title type="html"><![CDATA[Escaperoom early access]]></title>
<link href="https://coconauts.net/blog/2017/08/07/escaperoom-v3/"/>
<updated>2017-08-07T20:00:14+00:00</updated>
<id>https://coconauts.net/blog/2017/08/07/escaperoom-v3</id>
<content type="html"><![CDATA[<p>A couple of months ago, we showed you <a href="https://coconauts.net/blog/2017/05/29/bank-holiday-hackday-escape-room-on-unity3d/">Escaperoom</a>,
a puzzle game we developed during a hackday. We&rsquo;ve continued improving the game after that,
and today we want to show you a first polished prototype.</p>

<p><img src="/images/posts/2017-08-07-escaperoom/3dview-1.png" alt="3dview" /></p>

<p>This is not the finished game yet, but it&rsquo;s a version we consider polished enough
for playtesting. Give it a go here! <a href="http://unity3d.coconauts.net/escaperoom-v3/.">http://unity3d.coconauts.net/escaperoom-v3/.</a>
And read on for the geeky details&hellip;</p>

<!--more-->


<h2>The procedural puzzles puzzle</h2>

<p>After developing the hack, we tried to focus on the final goal of having dinamically generated rooms filled with parametric puzzles and multiple inputs/outputs; a sort of &ldquo;roghelike puzzler&rdquo;. We successfully wrote a first pass at an algoritm to
generate puzzle dependency charts, which was kind of convincing. However, we soon realized that it was difficult to
integrate the puzzles in a contextually coherent way.</p>

<p>Allow me to expand on this. Imagine you have a locked laptop, and to get the password you need to call a certain
telephone number, and the phone number shows up on a phone guide. If order for the player to make the connection, we
need to provide some context in the phone guide, such as the logo of your laptop. This is easy to do if you are
designing a static puzzle. However, in our case we have a generative algorithm that picks and matches clues and keys
with containers to hold them. In that way, the phone guide might be used to display any phone number, be it one to give out
the laptop password or another one to give you the color of a cable to cut.</p>

<p><img src="/images/posts/2017-08-07-escaperoom/phoneguide.png" alt="phoneguide" /></p>

<p>However, it&rsquo;s not a simple as it sounds. If you simply see a phone number with no context, you won&rsquo;t be able to know
where to use it, and the puzzle won&rsquo;t be fun. That&rsquo;s why we need to provide context alongside the phone number, that
will change depending on the generating puzzle (eg, the tech support service or the electrician), which might several
levels above in the puzzle dependency graph.</p>

<p>Our algorithm is not that smart yet. So we can get numbers and keys distributed, but without context, it is not fun.
To bring back the fun, we decided to step back and design a static puzzle to begin with, and we&rsquo;ll see into randomizing it later (or not!).</p>

<h2>Improvements</h2>

<p>So eventually, we decided reuse the puzzle design we crafted for the hackday, with mainly UI improvements.</p>

<p><img src="/images/posts/2017-08-07-escaperoom/graph.png" alt="room graph" /></p>

<p>These are all the new features of this new version:</p>

<ul>
<li>Text dialogs: usually by clicking in objects you will see some text , describing the object, and sometimes clues.</li>
<li>First person view and movement for the main room.</li>
<li>Puzzle hierarchy models, code refactoring, unity3d housekeeping&hellip;</li>
</ul>


<p><img src="/images/posts/2017-08-07-escaperoom/3dview-2.png" alt="3dview" /></p>

<p>We&rsquo;ve also experimented with some ideas that didn&rsquo;t make it into this version&hellip;</p>

<ul>
<li>New puzzles: gears, simon, keypad&hellip;</li>
<li>Augmented reality puzzles: QR codes</li>
<li>Rotating objects</li>
<li>Jigsaw generator (also works with qr codes)</li>
<li>Zooming objects</li>
<li>Dinamically generated room</li>
</ul>


<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Dynamically generated gears puzzle for our escape game <a href="https://twitter.com/hashtag/madewithunity?src=hash">#madewithunity</a> <a href="https://twitter.com/hashtag/gamedev?src=hash">#gamedev</a> Very happy with how it&#39;s progressing =D <a href="https://t.co/L3Ycdms7fz">pic.twitter.com/L3Ycdms7fz</a></p>&mdash; Javi Rengel (@rephus) <a href="https://twitter.com/rephus/status/875480874833367040">June 15, 2017</a></blockquote>


<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>


<h2>Conclussions</h2>

<p>Is funny how we spent a single day to develop the hackday version, but nearly two monthts (not full time) just on improving the current status and getting rid of the bugs.</p>

<p>Many of our trials and ideas didn&rsquo;t cut it into this version in the end, but might make it into future ones. We also
want to extend the puzzle to make it approximately one hour long, as escape room games usually are.</p>

<p>What do you think about the game? Were you able to escape the room? What did you like and what would you improve or change?
Your feedback is really valuable to us! so please let us know on the comments or <a href="http://twitter.com/coconauts">on twitter</a>.</p>
]]></content>
</entry>

<entry>
<title type="html"><![CDATA[RPS: A Phaser.io Simple RTS]]></title>
<link href="https://coconauts.net/blog/2017/02/18/rps-a-phaser-io-simple-rts/"/>
<updated>2017-02-18T11:57:19+00:00</updated>
<id>https://coconauts.net/blog/2017/02/18/rps-a-phaser-io-simple-rts</id>
<content type="html"><![CDATA[<p>If you follow <a href="http://twitter.com/rephus">me</a> or <a href="http://twitter.com/coconauts">Coconauts</a> on Twitter, you might have seen some
GIFs about an RTS game we&rsquo;re developing on Phaser.io.</p>

<p>Codename: RPS (Rock, Paper, Scissors). Is a simple RTS (Real Strategy Game)
we&rsquo;re building using the Phaser.io framework for HTML5 games.</p>

<p>We aim to have a fun and complete game, with campaign, multiplayer
and with a fresh interface, compatible with mobile and touch devices.</p>

<p>The main selling point are its simple but effective mechanics.
Forget about hundreds of different units with their own powerups, multiple buildings,
or advanced research.</p>

<!--more-->


<p>This is some of the recent progress we&rsquo;ve made in the last 2-3 weeks.</p>

<ul>
<li>Minimap</li>
</ul>


<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Added a Minimap (bottom-rigth) to our game. It was easier than I expected <a href="https://twitter.com/hashtag/gamedev?src=hash">#gamedev</a> <a href="https://twitter.com/hashtag/phaserIo?src=hash">#phaserIo</a> <a href="https://t.co/A0ATvb5ZxM">pic.twitter.com/A0ATvb5ZxM</a></p>&mdash; Coconauts (@coconauts) <a href="https://twitter.com/coconauts/status/826728173996015616">February 1, 2017</a></blockquote>


<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>


<ul>
<li>Simple AI, sight area, and new map</li>
</ul>


<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Big progress on our RTS game in <a href="https://twitter.com/hashtag/phaserio?src=hash">#phaserio</a>: simple AI, sight area, new map and more! <a href="https://twitter.com/hashtag/gamedev?src=hash">#gamedev</a> <a href="https://t.co/jnxwofvm5c">pic.twitter.com/jnxwofvm5c</a></p>&mdash; Coconauts (@coconauts) <a href="https://twitter.com/coconauts/status/826161939478872069">January 30, 2017</a></blockquote>


<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>


<ul>
<li>Emojis dialogs</li>
</ul>


<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Express yourself with in-game emojis <a href="https://twitter.com/hashtag/gamedev?src=hash">#gamedev</a> <a href="https://t.co/l6RoYpmrrm">pic.twitter.com/l6RoYpmrrm</a></p>&mdash; Javi Rengel (@rephus) <a href="https://twitter.com/rephus/status/825682597170786304">January 29, 2017</a></blockquote>


<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>


<p>If you want to follow these updates closely, checkout our <a href="/projects/rps">Project page</a></p>

<p>And let us know if you want to give us feedback about how to improve the game !</p>
]]></content>
</entry>

<entry>
<title type="html"><![CDATA[Updates on 404-games]]></title>
<link href="https://coconauts.net/blog/2015/08/08/404-updates/"/>
<updated>2015-08-08T22:00:00+00:00</updated>
<id>https://coconauts.net/blog/2015/08/08/404-updates</id>
<content type="html"><![CDATA[<p>Last week we implemented <a href="/projects/404games">gamified 404 pages</a> into
coconauts. Everytime you hit a 404, instead of getting a boring
image and cursing for your misfortune, you&rsquo;ll now get a classic videogame
to make you forget your disappointment.</p>

<p>We initially implemented pacman and space-invaders style games, but
we&rsquo;ve now added a snake game on top of them, and not only that, the
source code for them is now public on
<a href="https://github.com/coconauts/404-games">a github repo</a>, so you can
use them in your own website if you want.</p>

<p><a href="http://coconauts.net/404">Happy mislinking</a>.</p>
]]></content>
</entry>

<entry>
<title type="html"><![CDATA[404 Games]]></title>
<link href="https://coconauts.net/blog/2015/07/30/404-games/"/>
<updated>2015-07-30T23:52:00+00:00</updated>
<id>https://coconauts.net/blog/2015/07/30/404-games</id>
<content type="html"><![CDATA[<p>Getting a <a href="http://coconauts.net/404">404</a> page is not something enjoyable,
that&rsquo;s why we wanted to build something nice for anyone that lands in this
unexpected place.</p>

<p>So we built a few simple classic games in HTML5 + Canvas, and you will get randomly
one of these everytime you land in our <a href="http://coconauts.net/404">404</a> page.</p>

<!--more-->


<p>All our 404 games will be published in our <a href="/projects/404games">project page</a></p>

<h2>Space invaders</h2>

<p><img src="https://coconauts.net/projects/404games/space-invaders.png" class="screenshot" /></p>

<p><a href="http://coconauts.net/html5/space-invaders">Test me</a></p>

<h2>Pacman</h2>

<p><img src="https://coconauts.net/projects/404games/pacman.png" class="screenshot" /></p>

<p><a href="http://coconauts.net/html5/pacman">Test me</a></p>

<p>Fortunately, 404 pages don&rsquo;t happen too often in <a href="http://coconauts.net">Coconauts</a>,
but still, this is an improvement over the previous version,
and we had fun building this.</p>
]]></content>
</entry>

</feed>